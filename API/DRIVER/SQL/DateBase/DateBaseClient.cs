﻿using Abstraction.Entity;
using Data;
using SqLite.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SQL.DateBase
{
    class DateBaseClient : Abstraction.Database.IDateBaseClient
    {
        readonly string tableName = "Client";
        readonly string keyName = "Client_ID";

        public IClient CreateItem()
        {
            return new Client();
        }

        public IClient CreateOne()
        {
            return new Client();
        }

        public int Delete(int id)
        {
            SqLite.Data.DatabaseReadWrite<Client> delete = new DatabaseReadWrite<Client>();
            delete.EntityName = tableName;
            delete.EntityKey = keyName;
            return Convert.ToInt32(delete.Delete(id));
        }

        public List<IClient> get()
        {
            DataBaseReadAll<Client> readAll = new DataBaseReadAll<Client>();
            readAll.EntityName = tableName;
            List<IClient> result = new List<IClient>();
            result.AddRange(readAll.get());
            return result;

        }

        public IClient get(int id)
        {
            DataBaseReadOne<Abstraction.Entity.Client> readOne = new DataBaseReadOne<Client>();
            readOne.EntityName = tableName;
            readOne.EntityKey = keyName;
            Client result = readOne.get(id);
            return result;
        }

        public int Post(IClient value)
        {
            DatabaseReadWrite<Client> post = new DatabaseReadWrite<Client>();
            post.EntityName = tableName;
            post.EntityKey = keyName;
            post.Post((Abstraction.Entity.Client)value);
            return Convert.ToInt32(post.ResultМethod);

        }

        public int Put(int id, IClient value)
        {
            DatabaseReadWrite<Client> put = new DatabaseReadWrite<Client>();
            put.EntityName = tableName;
            put.EntityKey = keyName;
            put.Put(id, (Abstraction.Entity.Client)value);
            return Convert.ToInt32(put.ResultМethod);

        }
    }
}
