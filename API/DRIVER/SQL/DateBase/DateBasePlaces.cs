﻿using Abstraction.Entity;
using Data;
using SqLite.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace SQL.DateBase
{
    class DateBasePlaces : Abstraction.Database.IDateBasePlaces
    {
        readonly string tableName = "Places";
        readonly string keyName = "Places_ID";
        public IPlaces CreateOne()
        {
            throw new NotImplementedException();
        }

        public int Delete(int id)
        {
            SqLite.Data.DatabaseReadWrite<Places> delete = new DatabaseReadWrite<Places>();
            delete.EntityName = tableName;
            delete.EntityKey = keyName;
            return Convert.ToInt32(delete.Delete(id));
        }

        public List<IPlaces> get()
        {
            DataBaseReadAll<Places> readAll = new DataBaseReadAll<Places>();
            readAll.EntityName = tableName;
            List<IPlaces> result = new List<IPlaces>();
            result.AddRange(readAll.get());
            return result;

        }

        public IPlaces get(int id)
        {
            DataBaseReadOne<Places> readOne = new DataBaseReadOne<Places>();
            readOne.EntityName = tableName;
            readOne.EntityKey = keyName;
            Abstraction.Entity.Places result = readOne.get(id);
            return result;
        }

        public int Post(IPlaces value)
        {
            DatabaseReadWrite<Places> post = new DatabaseReadWrite<Places>();
            post.EntityName = tableName;
            post.EntityKey = keyName;
            post.Post((Abstraction.Entity.Places)value);
            return Convert.ToInt32(post.ResultМethod);

        }

        public int Put(int id, IPlaces value)
        {
            DatabaseReadWrite<Places> put = new DatabaseReadWrite<Places>();
            put.EntityName = tableName;
            put.EntityKey = keyName;
            put.Put(id, (Abstraction.Entity.Places)value);
            return Convert.ToInt32(put.ResultМethod);

        }
    }
}
