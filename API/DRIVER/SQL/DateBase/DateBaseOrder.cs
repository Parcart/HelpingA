﻿using Abstraction.Entity;
using Data;
using SqLite.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SQL.DateBase
{
    class DateBaseOrder : Abstraction.Database.IDateBaseOrder
    {
        readonly string tableName = "OrderA";
        readonly string keyName = "Order_ID";
        public IOrder CreateOne()
        {
            return new Order();
        }

        public int Delete(int id)
        {
            SqLite.Data.DatabaseReadWrite<Order> delete = new DatabaseReadWrite<Order>();
            delete.EntityName = tableName;
            delete.EntityKey = keyName;
            return Convert.ToInt32(delete.Delete(id));
        }

        public List<IOrder> get()
        {
            DataBaseReadAll<Order> readAll = new DataBaseReadAll<Order>();
            readAll.EntityName = tableName;
            List<IOrder> result = new List<IOrder>();
            result.AddRange(readAll.get());
            return result;

        }

        public IOrder get(int id)
        {
            DataBaseReadOne<Order> readOne = new DataBaseReadOne<Order>();
            readOne.EntityName = tableName;
            readOne.EntityKey = keyName;
            Abstraction.Entity.Order result = readOne.get(id);
            return result;
        }

        public int Post(IOrder value)
        {
            DatabaseReadWrite<Order> post = new DatabaseReadWrite<Order>();
            post.EntityName = tableName;
            post.EntityKey = keyName;
            post.Post((Abstraction.Entity.Order)value);
            return Convert.ToInt32(post.ResultМethod);

        }

        public int Put(int id, IOrder value)
        {
            DatabaseReadWrite<Order> put = new DatabaseReadWrite<Order>();
            put.EntityName = tableName;
            put.EntityKey = keyName;
            put.Put(id, (Abstraction.Entity.Order)value);
            return Convert.ToInt32(put.ResultМethod);

        }
    }
}
