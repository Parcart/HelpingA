﻿using Abstraction.Entity;
using Data;
using SqLite.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SQL.DateBase
{
    class DateBaseGroup : Abstraction.Database.IDateBaseGroup
    {
        readonly string tableName = "GroupD";
        readonly string keyName = "Group_ID";
        public IGroup CreateOne()
        {
            return new Group();
        }

        public int Delete(int id)
        {
            SqLite.Data.DatabaseReadWrite<Group> delete = new DatabaseReadWrite<Group>();
            delete.EntityName = tableName;
            delete.EntityKey = keyName;
            return Convert.ToInt32(delete.Delete(id));
        }

        public List<IGroup> get()
        {
            DataBaseReadAll<Group> readAll = new DataBaseReadAll<Group>();
            readAll.EntityName = tableName;
            List<IGroup> result = new List<IGroup>();
            result.AddRange(readAll.get());
            return result;

        }

        public IGroup get(int id)
        {
            DataBaseReadOne<Group> readOne = new DataBaseReadOne<Group>();
            readOne.EntityName = tableName;
            readOne.EntityKey = keyName;
            Abstraction.Entity.Group result = readOne.get(id);
            return result;
        }

        public int Post(IGroup value)
        {
            DatabaseReadWrite<Group> post = new DatabaseReadWrite<Group>();
            post.EntityName = tableName;
            post.EntityKey = keyName;
            post.Post((Abstraction.Entity.Group)value);
            return Convert.ToInt32(post.ResultМethod);

        }

        public int Put(int id, IGroup value)
        {
            DatabaseReadWrite<Group> put = new DatabaseReadWrite<Group>();
            put.EntityName = tableName;
            put.EntityKey = keyName;
            put.Put(id, (Abstraction.Entity.Group)value);
            return Convert.ToInt32(put.ResultМethod);

        }
    }
}
