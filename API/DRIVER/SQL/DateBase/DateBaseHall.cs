﻿using Abstraction.Entity;
using Data;
using SqLite.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SQL.DateBase
{
    class DateBaseHall : Abstraction.Database.IDateBaseHall
    {
        readonly string tableName = "Hall";
        readonly string keyName = "Hall_ID";

        public IHall CreateItem()
        {
            return new Hall();
        }

        public IHall CreateOne()
        {
            return new Hall();
        }

        public int Delete(int id)
        {
            SqLite.Data.DatabaseReadWrite<Hall> delete = new DatabaseReadWrite<Hall>();
            delete.EntityName = tableName;
            delete.EntityKey = keyName;
            return Convert.ToInt32(delete.Delete(id));
        }

        public List<IHall> get()
        {
            DataBaseReadAll<Hall> readAll = new DataBaseReadAll<Hall>();
            readAll.EntityName = tableName;
            List<IHall> result = new List<IHall>();
            result.AddRange(readAll.get());
            return result;

        }

        public IHall get(int id)
        {
            DataBaseReadOne<Hall> readOne = new DataBaseReadOne<Hall>();
            readOne.EntityName = tableName;
            readOne.EntityKey = keyName;
            Abstraction.Entity.Hall result = readOne.get(id);
            return result;
        }

        public int Post(IHall value)
        {
            DatabaseReadWrite<Hall> post = new DatabaseReadWrite<Hall>();
            post.EntityName = tableName;
            post.EntityKey = keyName;
            post.Post((Abstraction.Entity.Hall)value);
            return Convert.ToInt32(post.ResultМethod);

        }

        public int Put(int id, IHall value)
        {
            DatabaseReadWrite<Hall> put = new DatabaseReadWrite<Hall>();
            put.EntityName = tableName;
            put.EntityKey = keyName;
            put.Put(id, (Abstraction.Entity.Hall)value);
            return Convert.ToInt32(put.ResultМethod);

        }
    }
}
