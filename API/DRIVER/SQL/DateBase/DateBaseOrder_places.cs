﻿using Abstraction.Entity;
using Data;
using SqLite.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SQL.DateBase
{
    class DateBaseOrder_places : Abstraction.Database.IDateBaseOrder_places
    {
        readonly string tableName = "Order_places";
        readonly string keyName = "ID";
        public IOrder_places CreateOne()
        {
            throw new NotImplementedException();
        }

        public int Delete(int id)
        {
            SqLite.Data.DatabaseReadWrite<Order_places> delete = new DatabaseReadWrite<Order_places>();
            delete.EntityName = tableName;
            delete.EntityKey = keyName;
            return Convert.ToInt32(delete.Delete(id));
        }

        public List<IOrder_places> get()
        {
            DataBaseReadAll<Order_places> readAll = new DataBaseReadAll<Order_places>();
            readAll.EntityName = tableName;
            List<IOrder_places> result = new List<IOrder_places>();
            result.AddRange(readAll.get());
            return result;

        }

        public IOrder_places get(int id)
        {
            DataBaseReadOne<Order_places> readOne = new DataBaseReadOne<Order_places>();
            readOne.EntityName = tableName;
            readOne.EntityKey = keyName;
            Abstraction.Entity.Order_places result = readOne.get(id);
            return result;
        }

        public int Post(IOrder_places value)
        {
            DatabaseReadWrite<Order_places> post = new DatabaseReadWrite<Order_places>();
            post.EntityName = tableName;
            post.EntityKey = keyName;
            post.Post((Abstraction.Entity.Order_places)value);
            return Convert.ToInt32(post.ResultМethod);

        }

        public int Put(int id, IOrder_places value)
        {
            DatabaseReadWrite<Order_places> put = new DatabaseReadWrite<Order_places>();
            put.EntityName = tableName;
            put.EntityKey = keyName;
            put.Put(id, (Abstraction.Entity.Order_places)value);
            return Convert.ToInt32(put.ResultМethod);

        }
    }
}
