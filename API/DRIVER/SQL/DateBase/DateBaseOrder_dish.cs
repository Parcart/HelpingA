﻿using Abstraction.Entity;
using Data;
using SqLite.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace SQL.DateBase
{
    class DateBaseOrder_dish : Abstraction.Database.IDateBaseOrder_dish
    {
        readonly string tableName = "Order_dish";
        readonly string keyName = "id";
        public IOrder_dish CreateOne()
        {
            return new Order_dish(); 
        }

        public int Delete(int id)
        {
            SqLite.Data.DatabaseReadWrite<Order_dish> delete = new DatabaseReadWrite<Order_dish>();
            delete.EntityName = tableName;
            delete.EntityKey = keyName;
            return Convert.ToInt32(delete.Delete(id));
        }

        public List<IOrder_dish> get()
        {
            DataBaseReadAll<Order_dish> readAll = new DataBaseReadAll<Order_dish>();
            readAll.EntityName = tableName;
            List<IOrder_dish> result = new List<IOrder_dish>();
            result.AddRange(readAll.get());
            return result;

        }

        public IOrder_dish get(int id)
        {
            DataBaseReadOne<Order_dish> readOne = new DataBaseReadOne<Order_dish>();
            readOne.EntityName = tableName;
            readOne.EntityKey = keyName;
            Abstraction.Entity.Order_dish result = readOne.get(id);
            return result;
        }

        public int Post(IOrder_dish value)
        {
            DatabaseReadWrite<Order_dish> post = new DatabaseReadWrite<Order_dish>();
            post.EntityName = tableName;
            post.EntityKey = keyName;
            post.Post((Abstraction.Entity.Order_dish)value);
            return Convert.ToInt32(post.ResultМethod);

        }

        public int Put(int id, IOrder_dish value)
        {
            DatabaseReadWrite<Order_dish> put = new DatabaseReadWrite<Order_dish>();
            put.EntityName = tableName;
            put.EntityKey = keyName;
            put.Put(id, (Abstraction.Entity.Order_dish)value);
            return Convert.ToInt32(put.ResultМethod);

        }
    }
}
