﻿using Abstraction.Entity;
using Data;
using SqLite.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SQL.DateBase
{
    class DateBaseDish : Abstraction.Database.IDateBaseDish
    {
        readonly string tableName = "Dish";
        readonly string keyName = "Dish_ID";

        public IDish CreateOne()
        {
            return new Dish();
        }

        public int Delete(int id)
        {
            SqLite.Data.DatabaseReadWrite<Dish> delete = new DatabaseReadWrite<Dish>();
            delete.EntityName = tableName;
            delete.EntityKey = keyName;
            return Convert.ToInt32(delete.Delete(id));
        }

        public List<IDish> get()
        {
            DataBaseReadAll<Dish> readAll = new DataBaseReadAll<Dish>();
            readAll.EntityName = tableName;
            List<IDish> result = new List<IDish>();
            result.AddRange(readAll.get());
            return result;

        }

        public IDish get(int id)
        {
            DataBaseReadOne<Dish> readOne = new DataBaseReadOne<Dish>();
            readOne.EntityName = tableName;
            readOne.EntityKey = keyName;
            Abstraction.Entity.Dish result = readOne.get(id);
            return result;
        }

        public int Post(IDish value)
        {
            DatabaseReadWrite<Dish> post = new DatabaseReadWrite<Dish>();
            post.EntityName = tableName;
            post.EntityKey = keyName;
            post.Post((Abstraction.Entity.Dish)value);
            return Convert.ToInt32(post.ResultМethod);

        }

        public int Put(int id, IDish value)
        {
            DatabaseReadWrite<Dish> put = new DatabaseReadWrite<Dish>();
            put.EntityName = tableName;
            put.EntityKey = keyName;
            put.Put(id, (Abstraction.Entity.Dish)value);
            return Convert.ToInt32(put.ResultМethod);

        }
    }
}
