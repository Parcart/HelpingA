﻿using Abstraction.Entity;
using Data;
using SqLite.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SQL.DateBase
{
    class DateBaseWaiters : Abstraction.Database.IDateBaseWaiters
    {
        readonly string tableName = "Waiters";
        readonly string keyName = "Waiter_id";

        public IWaiters CreateItem()
        {
            return new Waiters();
        }

        public IWaiters CreateOne()
        {
            return new Waiters();
        }

        public int Delete(int id)
        {
            SqLite.Data.DatabaseReadWrite<Waiters> delete = new DatabaseReadWrite<Waiters>();
            delete.EntityName = tableName;
            delete.EntityKey = keyName;
            return Convert.ToInt32(delete.Delete(id));
        }

        public List<IWaiters> get()
        {
            DataBaseReadAll<Waiters> readAll = new DataBaseReadAll<Waiters>();
            readAll.EntityName = tableName;
            List<IWaiters> result = new List<IWaiters>();
            result.AddRange(readAll.get());
            return result;

        }

        public IWaiters get(int id)
        {
            DataBaseReadOne<Abstraction.Entity.Waiters> readOne = new DataBaseReadOne<Waiters>();
            readOne.EntityName = tableName;
            readOne.EntityKey = keyName;
            Waiters result = readOne.get(id);
            return result;
        }

        public int Post(IWaiters value)
        {
            DatabaseReadWrite<Waiters> post = new DatabaseReadWrite<Waiters>();
            post.EntityName = tableName;
            post.EntityKey = keyName;
            post.Post((Abstraction.Entity.Waiters)value);
            return Convert.ToInt32(post.ResultМethod);

        }

        public int Put(int id, IWaiters value)
        {
            DatabaseReadWrite<Waiters> put = new DatabaseReadWrite<Waiters>();
            put.EntityName = tableName;
            put.EntityKey = keyName;
            put.Put(id, (Abstraction.Entity.Waiters)value);
            return Convert.ToInt32(put.ResultМethod);

        }
    }
}
