﻿using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Data.Sqlite;
using SqLite.Data;
using SQL.Data;

namespace SQL.DateBase
{
    internal class DatabaseUsers : DatabaseReadOnly<Abstraction.Entity.Users>,
        Abstraction.Database.IDateBaseReadOne<Abstraction.Entity.Users>,
        Abstraction.Database.IDateBaseUsers
    {
        readonly string tableName = "Users";
        public Users GetByUserName(string userName)
        {
            Users result = new Users();
            //result.Password = "F9A6CAE0DDC234DE486680846485843E";
            //result.ValidatePassword("123");
            if (DAO.Instance.isConnection())
            {
               
                try
                {
                    List<SqliteParameter> sqliteParameters = new List<SqliteParameter>();
                    sqliteParameters.Add(
                        new SqliteParameter(
                            "$userName",
                            userName
                        )
                    );
                    Dictionary<string, List<SqliteParameter>> dict = new Dictionary<string, List<SqliteParameter>>();
                    dict.Add($"SELECT * FROM {tableName} WHERE UserName=$userName", sqliteParameters);
                    List<Users> queryResult = DAO.Instance.GetAll<Users>(dict);

                    if (queryResult.Count > 0)
                    {
                        result = queryResult[0];
                    }
                }
                catch (Exception)
                {
                    ;
                }
            }
            return result;
        }
    }
}
