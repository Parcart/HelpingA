﻿using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;

#nullable enable
namespace SQL.Data
{
    internal class DAO
    {
        private SqliteConnection connection;
        private Mutex connectionUseMutex;
        private static DAO? instance = null;
        public static DAO Instance
        {
            get
            {
                if (instance == null)
                {
                    throw (new Exception("setting not set"));
                }
                else
                {
                    return instance;
                }
            }
        }
        public bool isConnection()
        {
            if (instance != null)
            {
                return true;
            }
            return false;
        }
        public static bool Connect(Abstraction.IDatabaseSettings settings)
        {
            bool result = false;
            instance = null;
            try
            {
                instance = new DAO(settings);
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }

        private DAO(Abstraction.IDatabaseSettings settings)
        {
            connection = new SqliteConnection();
            connection.ConnectionString = $"Data Source={settings.Location}";
            connection.Open();
            connection.Close();
            connectionUseMutex = new Mutex();
        }
        public List<T> GetAll<T>(Dictionary<string, List<SqliteParameter>> textAndParametrs) where T : class, new()
        {
            List<T> result = new List<T>();
            try
            {
                connectionUseMutex.WaitOne();
                SqliteCommand command = connection.CreateCommand();
                connection.Open();
                command.CommandText = textAndParametrs.First().Key;
                command.Parameters.AddRange(textAndParametrs.First().Value);
                SqliteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(
                        FillFromRow<T>(ref reader)
                    );
                }
                connection.Close();
                connectionUseMutex.ReleaseMutex();
            }
            catch
            {
                connectionUseMutex.ReleaseMutex();
                throw new Exception("Failed at GetAll<T>");
            }
            return result;
        }


        public object? GetSingle(Dictionary<string, List<SqliteParameter>> textAndParametrs)
        {
            object? result = null;
            try
            {
                connectionUseMutex.WaitOne();
                SqliteCommand command = connection.CreateCommand();
                connection.Open();
                command.CommandText = textAndParametrs.First().Key;
                command.Parameters.AddRange(textAndParametrs.First().Value);
                result = command.ExecuteScalar();
                connection.Close();
                connectionUseMutex.ReleaseMutex();
            }
            catch
            {
                connectionUseMutex.ReleaseMutex();
                throw new Exception("Failed at GetSingle<T>");
            }
            return result;
        }

        private T FillFromRow<T>(ref SqliteDataReader reader) where T : class, new()
        {
            string formatData = "";
            T result = new T();
            PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            int cnt = reader.FieldCount;
            for (int i = 0; i < cnt; i++)
            {
                string fieldName = reader.GetName(i).ToLower();
                foreach (PropertyInfo property in properties)
                {
                    if (property.Name.ToLower() == fieldName)
                    {
                        if (reader.IsDBNull(i))
                        {
                            // Обработка DBNull, можно присвоить null или значение по умолчанию
                            if (property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                            {
                                property.SetValue(result, null);
                                break;
                            }

                        }
                        if (property.PropertyType.FullName.Contains("DateTime"))
                        {
                            string dataValueString = reader.GetValue(i).ToString();
                            DateTime myDate = DateTime.ParseExact(dataValueString, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                            property.SetValue(result, myDate);
                            continue;
                        }
                        property.SetValue(result, reader.GetValue(i));
                        break;
                    }
                }
            }
            return result;
        }
        public int? ExecuteNonQuery(string query)
        {
            int? result = null;
            try
            {
                connectionUseMutex.WaitOne();
                SqliteCommand command = connection.CreateCommand();
                connection.Open();
                command.CommandText = query;
                result = command.ExecuteNonQuery();
                connection.Close();
                connectionUseMutex.ReleaseMutex();
            }
            catch
            {
                connectionUseMutex.ReleaseMutex();
                throw new Exception("Failed at ExecuteNonQuery");
            }
            return result;

        }
    }
}

