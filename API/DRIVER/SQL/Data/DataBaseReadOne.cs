﻿using Abstraction.Entity;
using Microsoft.Data.Sqlite;
using SQL.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace SqLite.Data
{
    public class DataBaseReadOne<T> : DataBaseReadAll<T>, Abstraction.Database.IDateBaseReadOne<T> where T : class, new()
    {
        public T get(int id)
        {
            T result = new T();
            if (DAO.Instance.isConnection())
            {
                _parameters.Add(new SqliteParameter("KeyValue", id));
                List<T> queryResult = DAO.Instance.GetAll<T>(SqliteEditor<T>.AddGetOneCommand(EntityKey,EntityName,id));
                if (queryResult.Count > 0)
                {
                    result = queryResult[0];
                }
            }
            return result;
        }
       
        
        
    }
}