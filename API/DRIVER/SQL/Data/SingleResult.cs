﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQL.Data
{
    internal class SingleResult<T>
    {
        public T Value { get; set; }
        public SingleResult()
        {
            Value = default;
        }
    }
}

