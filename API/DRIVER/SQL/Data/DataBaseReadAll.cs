﻿using Microsoft.Data.Sqlite;
using SQL.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace SqLite.Data
{
    public class DataBaseReadAll<T> : Abstraction.Database.IDateBaseReadAll<T> where T : class, new()
    {
        private string _tableName = "";
        private string _entityKey = "";
        private string _resultМethod = "";
        public List<SqliteParameter> _parameters = new List<SqliteParameter>();

        public virtual string EntityName { get { return _tableName; } set { _tableName = value; } }
        public virtual string EntityKey { get { return _entityKey; } set { _entityKey = value; } }
        public virtual string ResultМethod { get { return _resultМethod; } set { _resultМethod = value; } }


        public List<T> get()
        {
            List<T> result = new List<T>();
            if (DAO.Instance.isConnection())
            {
                result = DAO.Instance.GetAll<T>(SqliteEditor<T>.AddGetAllCommand(EntityName));
            }
            return result;
        }
    }
}
