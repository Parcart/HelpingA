﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SQL.Data
{
    internal class DatabaseDDL
    {
        public const string Client = "CREATE TABLE Client (Client_ID INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL, FIO VARCHAR(75) NOT NULL, Discount INTEGER NOT NULL, HappyB DATE NOT NULL, TelephoneNumber TEXT (20) NOT NULL)";

        public const string Dish = "CREATE TABLE Dish (Dish_ID INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL, Name VARCHAR(75) NOT NULL, Price DOUBLE NOT NULL, Group_ID INTEGER REFERENCES GroupD(Group_ID) NOT NULL)";

        public const string GroupD = "CREATE TABLE GroupD (Group_ID INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL, Name VARCHAR(75) NOT NULL)";

        public const string Hall = "CREATE TABLE Hall (Hall_ID INTEGER NOT NULL UNIQUE PRIMARY KEY, Name VARCHAR(75) NOT NULL)";

        public const string OrderA = "CREATE TABLE OrderA (\r\n    Order_ID         INTEGER       PRIMARY KEY AUTOINCREMENT\r\n                                   UNIQUE\r\n                                   NOT NULL,\r\n    Data             DATE          NOT NULL,\r\n    Waiter_ID        INTEGER       NOT NULL\r\n                                   REFERENCES Waiters (Waiter_iD),\r\n    TotalPrice       DOUBLE        NOT NULL,\r\n    complite         CHAR (1)      NOT NULL,\r\n    IsReservation    CHAR (1)      NOT NULL,\r\n    client_id        INTEGER       REFERENCES Client (Client_ID),\r\n    clientName       VARCHAR (125),\r\n    DateReservation  DATE,\r\n    OrderReservStart CHAR (1)      NOT NULL\r\n)";

        public const string Order_dish = "CREATE TABLE Order_dish (\r\n    ID       INTEGER PRIMARY KEY AUTOINCREMENT\r\n                     NOT NULL\r\n                     UNIQUE,\r\n    Order_ID INTEGER NOT NULL\r\n                     REFERENCES OrderA (Order_ID),\r\n    Dish_ID  INTEGER NOT NULL\r\n                     REFERENCES Dish (Dish_ID),\r\n    Amount   INTEGER NOT NULL,\r\n    price    DOUBLE  NOT NULL\r\n)";

        public const string Order_places = "CREATE TABLE Order_places (ID INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL, Order_ID INTEGER NOT NULL REFERENCES OrderA (Order_ID), Places_ID INTEGER NOT NULL REFERENCES Places(Places_ID))";

        public const string Places = "CREATE TABLE Places (Places_ID INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL, stolik VARCHAR (32) NOT NULL, Hall_ID INTEGER NOT NULL REFERENCES Hall(Hall_ID))";

        public const string Reservation = "CREATE TABLE Reservation (\r\n    Reservation_ID INTEGER       PRIMARY KEY AUTOINCREMENT\r\n                                 UNIQUE\r\n                                 NOT NULL,\r\n    Data           DATE          NOT NULL,\r\n    client_id      INTEGER       REFERENCES Client (Client_ID),\r\n    Order_ID       INTEGER       NOT NULL\r\n                                 REFERENCES [Order] (Order_ID),\r\n    clientName     VARCHAR (125) \r\n)";

        public const string Waiters = "CREATE TABLE Waiters (Waiter_iD INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL, FIO VARCHAR(75) NOT NULL, HappyB DATE NOT NULL, TelephoneNumber TEXT (20) NOT NULL)";
        public const string Users = "CREATE TABLE Users (Users_ID INTEGER PRIMARY KEY AUTOINCREMENT, UserName VARCHAR (32) NOT NULL UNIQUE, Password VARCHAR (32) NOT NULL)";


        public static string Parse(string DDL)
        {
            string result = DDL.Replace("\n", "");
            result = result.Replace("\r", "");
            while (result.Contains("  ")) { result = result.Replace("  ", " "); } // Удаление лишних пробелов
            return result;
        }


        public readonly static Dictionary<string, string> Tables = new Dictionary<string, string>
        {
            {"Client", Client },
            {"Dish", Dish },
            {"Hall", Hall},
            {"GroupD", GroupD},
            {"OrderA", OrderA },
            {"Order_dish", Order_dish },
            {"Order_places", Order_places  },
            {"Places", Places },
            {"Reservation", Reservation },
            {"Waiters", Waiters},
            {"Users", Users}
        };


    }
}
