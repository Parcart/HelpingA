﻿using Abstraction.Database;
using Abstraction.Entity;
using SQL.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace SqLite.Data
{
    internal class DatabaseReadOnly<T> :
        SqLite.Data.DataBaseReadAll<T>,
        Abstraction.Database.IDateBaseUsers
        where T : class, new()
    {
        public Users CreateOne()
        {
            throw new NotImplementedException();
        }

        public int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Users get(int id)
        {
            throw new NotImplementedException();
        }

        public Users GetByUserName(string userName)
        {
            Users result = new Users();
            if (DAO.Instance.isConnection())
            {
                try
                {
                    
                    List<Users> queryResult = DAO.Instance.GetAll<Users>(SqliteEditor<Users>.ReadOnlyCommand(EntityKey, EntityName, userName));
                    if (queryResult.Count > 0)
                    {
                        result = queryResult[0];
                    }
                }
                catch (Exception)
                {
                    ;
                }
            }
            return result;
        }

        public int Post(Users value)
        {
            throw new NotImplementedException();
        }

        public int Put(int id, Users value)
        {
            throw new NotImplementedException();
        }

        List<Users> IDateBaseReadAll<Users>.get()
        {
            throw new NotImplementedException();
        }
    }
}
