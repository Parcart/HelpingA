﻿using Abstraction.Database;
using Microsoft.Data.Sqlite;
using SQL.Data;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.PortableExecutable;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace SqLite.Data
{
    public class DatabaseReadWrite<T> : DataBaseReadOne<T>, Abstraction.Database.IDateBaseReadWrite<T> where T : class, new()
    {

        public T CreateOne()
        {
            return new T();
        }

        public int Delete(int id)
        {
            try
            {

                object? result = DAO.Instance.GetSingle(SqliteEditor<T>.AddDeleteCommand(EntityKey, EntityName, id));
                ResultМethod = $"Удален id {id}";
                return Convert.ToInt32(result);
            }
            catch
            {
                throw new NotImplementedException("Записать с пользовательским ID существует");
            }
        }

        public int Post(T value)
        {
            try
            {
                object? result = DAO.Instance.GetSingle(SqliteEditor<T>.AddInsertCommand(value, EntityKey, EntityName, SqliteEditor<T>.SelectMetod.Post));
                if (result != null)
                {
                    ResultМethod = Convert.ToString(result);
                }
                return Convert.ToInt32(result);
            }
            catch
            {
                throw new NotImplementedException("Не удалось вставить записать");
            }
        }

        public int Put(int id, T value)
        {
            try
            {
                object? result = DAO.Instance.GetSingle(SqliteEditor<T>.AddUpdateCommand(value, EntityKey, EntityName, id, SqliteEditor<T>.SelectMetod.Put));
                if (result != null)
                {
                    ResultМethod = Convert.ToString(result);
                }
                return Convert.ToInt32(result);
            }
            catch
            {
                throw new NotImplementedException("Неудалось вставить запись, возможно пользовательский ID существует)");
            }
        }
    }
}

