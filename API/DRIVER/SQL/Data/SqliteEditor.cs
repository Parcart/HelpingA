﻿using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SqLite.Data
{
    public class SqliteEditor<T> where T : class, new()
    {
        private const string keysSepareator = ", ";
        public static Dictionary<string, List<SqliteParameter>> AddInsertCommand(T value, string EntityKey, string EntityName, SelectMetod selectMetod)
        {
            Dictionary<string, List<SqliteParameter>> ResultCommand = new Dictionary<string, List<SqliteParameter>>();
            int id = 0;
            List<SqliteParameter> parameters = AddParametrs(value, id, selectMetod, EntityKey);
            string keys = "";
            string values = "";
            parameters.ForEach(p =>
            {
                keys += $"{p.ParameterName}{keysSepareator}";
                values += $"${p.ParameterName}{keysSepareator}";
            });
            keys = RemoveLastKeySeparator(keys);
            values = RemoveLastKeySeparator(values);
            string sqlText = $"INSERT INTO {EntityName} ({keys}) VALUES({values}); select last_insert_rowid()";
            ResultCommand.Add(sqlText, parameters);
            return ResultCommand;
        }
        public static Dictionary<string, List<SqliteParameter>> AddUpdateCommand(T value, string EntityKey, string EntityName, int id, SelectMetod selectMetod)
        {
            Dictionary<string, List<SqliteParameter>> ResultCommand = new Dictionary<string, List<SqliteParameter>>();

            List<SqliteParameter> parameters = AddParametrs(value, id, selectMetod, EntityKey);
            string text = "";

            parameters.ForEach(p =>
            {
                text += $"{p.ParameterName} = ${p.ParameterName}{keysSepareator}";
            });
            text = RemoveLastKeySeparator(text);
            string sqlText = $"UPDATE {EntityName} SET {text} WHERE {EntityKey} = {id};  select changes()";
            ResultCommand.Add(sqlText, parameters);
            return ResultCommand;
        }
        public static Dictionary<string, List<SqliteParameter>> AddDeleteCommand(string EntityKey, string EntityName, int id)
        {
            List<SqliteParameter> parameters = new List<SqliteParameter>();
            Dictionary<string, List<SqliteParameter>> result = new Dictionary<string, List<SqliteParameter>>();
            string sqlText = $"DELETE FROM {EntityName} WHERE {EntityKey}={id}; select changes()";
            result.Add(sqlText, parameters);
            return result;
        }
        public static Dictionary<string, List<SqliteParameter>> ReadOnlyCommand(string EntityKey, string EntityName, string login)
        {
            List<SqliteParameter> parameters = new List<SqliteParameter>();
            Dictionary<string, List<SqliteParameter>> result = new Dictionary<string, List<SqliteParameter>>();
            string sqlText = $"SELECT * FROM {EntityName} WHERE login={login}";
            result.Add(sqlText, parameters);
            return result;
        }
        public static Dictionary<string, List<SqliteParameter>> AddGetAllCommand(string EntityName)
        {

            List<SqliteParameter> parameters = new List<SqliteParameter>();
            Dictionary<string, List<SqliteParameter>> result = new Dictionary<string, List<SqliteParameter>>();
            string sqlText = $"SELECT * FROM {EntityName}";
            result.Add(sqlText, parameters);
            return result;

        }
        public static Dictionary<string, List<SqliteParameter>> AddGetOneCommand(string EntityKey, string EntityName, int id)
        {
            if (EntityName == "Price")
            {
                List<SqliteParameter> parameters = new List<SqliteParameter>();
                Dictionary<string, List<SqliteParameter>> result = new Dictionary<string, List<SqliteParameter>>();
                string sqlText = $"SELECT Product.Price FROM Product WHERE Product.Poduct_id={id};";
                result.Add(sqlText, parameters);
                return result;
            }
            else
            {
                List<SqliteParameter> parameters = new List<SqliteParameter>();
                parameters.Add(new SqliteParameter("KeyValue", id));
                Dictionary<string, List<SqliteParameter>> result = new Dictionary<string, List<SqliteParameter>>();
                string sqlText = $"SELECT * FROM {EntityName} WHERE {EntityKey}=$KeyValue;";
                result.Add(sqlText, parameters);
                return result;
            }
        }
        private static List<SqliteParameter> AddParametrs(T value, int id, SelectMetod selectMetod, string key, bool nullable = false)
        {
            List<SqliteParameter> parameters = new List<SqliteParameter>();
            PropertyInfo[] properties = value.GetType().GetProperties();
            int i = 0;
            foreach (PropertyInfo property in properties)
            {
                if (selectMetod == SelectMetod.Post & i == 0)
                {
                    i++;
                    continue;
                }
                if (property.Name.ToString().ToLower() != key.ToLower())
                {
                    if (property.Name.ToString() == "NullString")
                    {
                        continue;
                    }
                    if (Nullable.GetUnderlyingType(property.PropertyType) != null |
                        (property.PropertyType == typeof(string) && property.GetMethod?.CustomAttributes.Where(x => x.AttributeType.Name ==
                        "NullableAttribute").Count() == 0))
                    {
                        nullable = true;
                    }

                    object? item = property.GetValue(value);
                    if (item != null)
                    {
                        parameters.Add(new SqliteParameter(property.Name.ToString(), item));
                        nullable = false;
                    }
                    else if(item == null && nullable)
                    {
                        parameters.Add(new SqliteParameter(property.Name.ToString(), DBNull.Value));
                        nullable = false;
                    }
                    else
                    {
                        throw new Exception($"Значение параметра-{property.Name} не заполненно");
                    }
                }
            }


            return parameters;
        }
        public enum SelectMetod
        {
            Put,
            Post,
            Delete,
            Add
        }
        private static string RemoveLastKeySeparator(string value)
        {
            if (value.EndsWith(keysSepareator))
            {
                return value.Remove(value.Length - keysSepareator.Length, keysSepareator.Length);
            }
            else
            {
                return value;
            }
        }


    }
}
