﻿using Abstraction;
using Abstraction.Database;
using System;
using System.Collections.Generic;
using System.Text;
using SQL.Data;
using Microsoft.Data.Sqlite;
using SQL.DateBase;

namespace Data
{
    public class DataBase : Abstraction.IDateBase
    {
        public IDateBaseClient Client { get; } = new DateBaseClient();

        public IDateBaseDish Dish { get; } = new DateBaseDish();

        public IDateBaseGroup Group { get; } = new DateBaseGroup();

        public IDateBaseHall Hall { get; } = new DateBaseHall();

        public IDateBaseOrder Order { get; } = new DateBaseOrder();

        public IDateBaseOrder_dish Order_dish { get; } = new DateBaseOrder_dish();

        public IDateBaseOrder_places order_places { get; } = new DateBaseOrder_places();

        public IDateBasePlaces places { get; } = new DateBasePlaces();

        public IDateBaseUsers Users { get; } = new DatabaseUsers();
        public IDateBaseWaiters Waiters { get; } = new DateBaseWaiters();

        public DataBase(IDatabaseSettings settings)
        {
            if (DAO.Connect(settings))
            {
                if (CheckDatabase())
                {

                }
                else
                {
                    throw new Exception("Database mission failed(verif)");
                }
            }
            else
            {
                throw new Exception("Wrong settings");
            }
        }
        private bool CheckDatabase()
        {
            bool result = true;
            foreach (KeyValuePair<string, string> item in DatabaseDDL.Tables)
            {
                result = result && CheckDatabaseEntity(item.Key);
            }
            DAO dao = DAO.Instance;
            List<SqliteParameter> parameters = new List<SqliteParameter>();
            string textSQL = $"SELECT UserName FROM Users WHERE UserName='admin';";
            Dictionary<string, List<SqliteParameter>> command = new Dictionary<string, List<SqliteParameter>>();
            command.Add(textSQL, parameters);
            object? data = dao.GetSingle(command);
            if (data == null)
            {
                string textSQLinsertd = $"INSERT INTO Users (UserName,Password) VALUES ('admin', 'F9A6CAE0DDC234DE486680846485843E');";
                Dictionary<string, List<SqliteParameter>> commandinst = new Dictionary<string, List<SqliteParameter>>();
                commandinst.Add(textSQLinsertd, parameters);
                object? dataUser = dao.GetSingle(commandinst);
            }
            return result;
        }
        private bool CheckDatabaseEntity(string tableName)
        {
            bool result = false;
            Dictionary<string, string> DLL = DatabaseDDL.Tables;
            string dllname = DLL[key: tableName];
            DAO dao = DAO.Instance;
            List<SqliteParameter> parameters = new List<SqliteParameter>();
            string textSQL = $"SELECT name FROM sqlite_master WHERE type='table' AND name='{tableName}';";
            Dictionary<string, List<SqliteParameter>> command = new Dictionary<string, List<SqliteParameter>>();
            command.Add(textSQL, parameters);
            {
                object? exists = dao.GetSingle(command);
                if (exists == null)
                {
                    int? created = dao.ExecuteNonQuery(dllname);
                    if (created != null)
                    {
                        result = true;
                    }
                }
                else
                {
                    textSQL = $"SELECT sql FROM sqlite_schema WHERE name = '{tableName}';";
                    command = new Dictionary<string, List<SqliteParameter>>();
                    command.Add(textSQL, parameters);
                    object? data = dao.GetSingle(command);
                    if (data != null)
                    {
                        string ddl = DatabaseDDL.Parse(data.ToString());
                        result = ddl == DatabaseDDL.Parse(dllname);
                    }
                }
                return result;

            }
        }
    }
}

