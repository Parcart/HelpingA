using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Text.Json;
using System.Text.Json.Serialization;
using API.Data;
using Microsoft.EntityFrameworkCore;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            API.Data.Repository.LoadDriver(configuration);
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<AuthDbContext>(options =>
            {
                options.UseOpenIddict();
            }
            );
            services.AddOpenIddict()
                 .AddCore(options =>
                 {
                     options.UseEntityFrameworkCore().UseDbContext<AuthDbContext>();
                 })
                 .AddServer(options =>
                 {
                     options.SetTokenEndpointUris("connect/token");
                     options.AllowClientCredentialsFlow();
                     options.AllowPasswordFlow();
                     options.AddDevelopmentEncryptionCertificate()
                        .AddDevelopmentSigningCertificate();
                     options.UseAspNetCore()
                        .EnableTokenEndpointPassthrough()
                        .DisableTransportSecurityRequirement();
                     options.SetAccessTokenLifetime(TimeSpan.FromHours(12));
                     options.DisableAccessTokenEncryption();
                 })
                 .AddValidation(options =>
                 {
                     options.UseLocalServer();
                     options.UseAspNetCore();
                 });


            services.AddSwaggerGen();
            services.AddHostedService<CreateClientsHostedService>();
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = OpenIddict.Validation.AspNetCore.OpenIddictValidationAspNetCoreDefaults.AuthenticationScheme;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapDefaultControllerRoute();
            });
            app.UseSwagger();
            app.UseSwaggerUI();

            Abstraction.Entity.Users users = new Abstraction.Entity.Users();
            users.ValidatePassword("pass");
        }
    }
}