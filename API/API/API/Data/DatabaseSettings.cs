﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Data
{
        public class DatabaseSettings : Abstraction.IDatabaseSettings
        {
            public string Location { get; set; }
            public string Database { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }
            public DatabaseSettings()
            {
                Location = "";
                Database = "";
                UserName = "";
                Password = "";
            }
        }
}
