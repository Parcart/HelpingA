﻿using Abstraction;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using static API.Data.DatabaseSettings;

#nullable enable
namespace API.Data
{
    public class Repository
    {
        private static Abstraction.IDateBase? database = null;
        private static IDatabaseSettings setting =new DatabaseSettings();
        public static Abstraction.IDateBase Instance
        {
            get
            {
                if (database == null)
                {
                    throw new InvalidOperationException("Database driver not loaded!");
                }
                else
                {
                    return database;
                }
            }
        }
        public static bool LoadDriver(IConfiguration configuration)
        {
            bool result = false;
            string drivername = "";
            try
            {
                drivername = configuration["DatabaseSettings:Driver"];
                drivername = GetAssemblyPath();
                setting.Location = configuration["DatabaseSettings:Location"];
                setting.UserName = configuration["DatabaseSettings:UserName"];
                setting.Password = configuration["DatabaseSettings:Password"];
                setting.Database = configuration["DatabaseSettings:Database"];
            }
            catch (Exception)
            {
                ;
            }
            if (File.Exists(drivername))
            {
                try
                {
                    byte[] data = System.IO.File.ReadAllBytes(drivername);
                    Assembly driver = Assembly.Load(data);
                    foreach (Type type in driver.GetExportedTypes())
                    {
                        if (type.Name == "DataBase")
                        {
                            object[] param = new object[1];
                            param[0] = setting;
                            object? instance = Activator.CreateInstance(type, param);
                            if (instance != null)
                            {
                                database = (Abstraction.IDateBase)instance;
                            }

                        }
                    }
                }
                catch (Exception)
                {
                    ;
                }
            }

            return result;
        }
        public static bool IsLoad
        {
            get => database != null;
        }
        private static string GetAssemblyPath()
        {
            string? result = null;
            string path = Path.GetFullPath(Path.Combine(Assembly.GetExecutingAssembly().Location, @"..\..\..\..\..\..\"));
            result = path + @"DRIVER\SQL\bin\Debug\netcoreapp3.1\SQL.dll";
            return result;
        }
    }
}

