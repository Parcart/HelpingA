﻿using Microsoft.AspNetCore.Mvc;
using Abstraction.Entity;
using System.Collections.Generic;
using API.Data;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Order_dishController : ControllerBase
    {
        // GET: api/<Order_dishController>
        [HttpGet]
        public IEnumerable<IOrder_dish> Get()
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Order_dish.get();
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // GET api/<Order_dishController>/5
        [HttpGet("{id}")]
        public IOrder_dish Get(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Order_dish.get(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // POST api/<Order_dishController>
        [HttpPost]
        public int Post([FromBody] Order_dish value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Order_dish.Post(value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // PUT api/<Order_dishController>/5
        [HttpPut("{id}")]
        public int Put(int id, [FromBody] Order_dish value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Order_dish.Put(id, value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // DELETE api/<Order_dishController>/5
        [HttpDelete("{id}")]
        public int Delete(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Order_dish.Delete(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }
    }
}

