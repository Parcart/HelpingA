﻿using Microsoft.AspNetCore.Mvc;
using Abstraction.Entity;
using System.Collections.Generic;
using API.Data;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HallController : ControllerBase
    {
        // GET: api/<HallController>
        [HttpGet]
        public IEnumerable<IHall> Get()
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Hall.get();
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // GET api/<HallController>/5
        [HttpGet("{id}")]
        public IHall Get(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Hall.get(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // POST api/<HallController>
        [HttpPost]
        public int Post([FromBody] Hall value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Hall.Post(value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // PUT api/<HallController>/5
        [HttpPut("{id}")]
        public int Put(int id, [FromBody] Hall value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Hall.Put(id, value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // DELETE api/<HallController>/5
        [HttpDelete("{id}")]
        public int Delete(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Hall.Delete(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }
    }
}

