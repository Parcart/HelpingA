﻿using Microsoft.AspNetCore.Mvc;
using Abstraction.Entity;
using System.Collections.Generic;
using API.Data;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        // GET: api/<ClientController>
        [HttpGet]
        public IEnumerable<IClient> Get()
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Client.get();
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // GET api/<ClientController>/5
        [HttpGet("{id}")]
        public IClient Get(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Client.get(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // POST api/<ClientController>
        [HttpPost]
        public int Post([FromBody] Client value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Client.Post(value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // PUT api/<ClientController>/5
        [HttpPut("{id}")]
        public int Put(int id, [FromBody] Client value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Client.Put(id, value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // DELETE api/<ClientController>/5
        [HttpDelete("{id}")]
        public int Delete(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Client.Delete(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }
    }
}

