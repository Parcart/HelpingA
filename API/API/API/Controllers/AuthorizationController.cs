﻿using Abstraction.Entity;
using API.Data;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using OpenIddict.Abstractions;
using OpenIddict.Server.AspNetCore;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using static OpenIddict.Abstractions.OpenIddictConstants;

namespace API.Controllers
{
    public class AuthorizationController : Controller
    {
        private readonly IOpenIddictApplicationManager _applicationManager;

        public AuthorizationController(IOpenIddictApplicationManager applicationManager)
            => _applicationManager = applicationManager;

        [HttpPost("~/connect/token"), Produces("application/json")]
        public async Task<IActionResult> Exchange()
        {
            var request = HttpContext.GetOpenIddictServerRequest();
            if (request.IsPasswordGrantType())
            {
                Users user = Repository.Instance.Users.GetByUserName(request.Username);
                if (user.Users_ID == null)
                {
                    return Forbid(OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
                }
                else
                {
                    if (user.ValidatePassword(request.Password))
                    {
                        var identity = new ClaimsIdentity(
                            OpenIddictServerAspNetCoreDefaults.AuthenticationScheme,
                            OpenIddictConstants.Claims.Name,
                            OpenIddictConstants.Claims.Role);
                        identity.AddClaim(OpenIddictConstants.Claims.Subject,
                            user.UserName,
                            OpenIddictConstants.Destinations.AccessToken);
                        identity.AddClaim(OpenIddictConstants.Claims.Name, user.UserName,
                            OpenIddictConstants.Destinations.AccessToken);
                        ClaimsPrincipal principal = new ClaimsPrincipal(identity);
                        return SignIn(principal, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
                    }
                    else
                    {
                        return Forbid(OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
                    }
                }
            }
            throw new InvalidOperationException("The specified grant type is not 1 supported.");


        }
    }
}
