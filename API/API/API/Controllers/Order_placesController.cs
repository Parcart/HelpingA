﻿using Microsoft.AspNetCore.Mvc;
using Abstraction.Entity;
using System.Collections.Generic;
using API.Data;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Order_placesController : ControllerBase
    {
        // GET: api/<Order_placesController>
        [HttpGet]
        public IEnumerable<IOrder_places> Get()
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.order_places.get();
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // GET api/<Order_placesController>/5
        [HttpGet("{id}")]
        public IOrder_places Get(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.order_places.get(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // POST api/<Order_placesController>
        [HttpPost]
        public int Post([FromBody] Order_places value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.order_places.Post(value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // PUT api/<Order_placesController>/5
        [HttpPut("{id}")]
        public int Put(int id, [FromBody] Order_places value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.order_places.Put(id, value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // DELETE api/<Order_placesController>/5
        [HttpDelete("{id}")]
        public int Delete(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.order_places.Delete(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }
    }
}

