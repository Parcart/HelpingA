﻿using Microsoft.AspNetCore.Mvc;
using Abstraction.Entity;
using System.Collections.Generic;
using API.Data;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlacesController : ControllerBase
    {
        // GET: api/<PlacesController>
        [HttpGet]
        public IEnumerable<IPlaces> Get()
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.places.get();
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // GET api/<PlacesController>/5
        [HttpGet("{id}")]
        public IPlaces Get(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.places.get(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // POST api/<PlacesController>
        [HttpPost]
        public int Post([FromBody] Places value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.places.Post(value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // PUT api/<PlacesController>/5
        [HttpPut("{id}")]
        public int Put(int id, [FromBody] Places value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.places.Put(id, value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // DELETE api/<PlacesController>/5
        [HttpDelete("{id}")]
        public int Delete(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.places.Delete(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }
    }
}

