﻿using Microsoft.AspNetCore.Mvc;
using Abstraction.Entity;
using System.Collections.Generic;
using API.Data;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WaitersController : ControllerBase
    {
        // GET: api/<WaitersController>
        [HttpGet]
        public IEnumerable<IWaiters> Get()
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Waiters.get();
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // GET api/<WaitersController>/5
        [HttpGet("{id}")]
        public IWaiters Get(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Waiters.get(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // POST api/<WaitersController>
        [HttpPost]
        public int Post([FromBody] Waiters value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Waiters.Post(value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // PUT api/<WaitersController>/5
        [HttpPut("{id}")]
        public int Put(int id, [FromBody] Waiters value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Waiters.Put(id, value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // DELETE api/<WaitersController>/5
        [HttpDelete("{id}")]
        public int Delete(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Waiters.Delete(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }
    }
}

