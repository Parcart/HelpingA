﻿using Microsoft.AspNetCore.Mvc;
using Abstraction.Entity;
using System.Collections.Generic;
using API.Data;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        // GET: api/<OrderController>
        [HttpGet]
        public IEnumerable<IOrder> Get()
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Order.get();
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // GET api/<OrderController>/5
        [HttpGet("{id}")]
        public IOrder Get(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Order.get(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // POST api/<OrderController>
        [HttpPost]
        public int Post([FromBody] Order value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Order.Post(value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // PUT api/<OrderController>/5
        [HttpPut("{id}")]
        public int Put(int id, [FromBody] Order value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Order.Put(id, value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // DELETE api/<OrderController>/5
        [HttpDelete("{id}")]
        public int Delete(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Order.Delete(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }
    }
}

