﻿using Microsoft.AspNetCore.Mvc;
using Abstraction.Entity;
using System.Collections.Generic;
using API.Data;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GroupController : ControllerBase
    {
        // GET: api/<GroupController>
        [HttpGet]
        public IEnumerable<IGroup> Get()
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Group.get();
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // GET api/<GroupController>/5
        [HttpGet("{id}")]
        public IGroup Get(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Group.get(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // POST api/<GroupController>
        [HttpPost]
        public int Post([FromBody] Group value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Group.Post(value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // PUT api/<GroupController>/5
        [HttpPut("{id}")]
        public int Put(int id, [FromBody] Group value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Group.Put(id, value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // DELETE api/<GroupController>/5
        [HttpDelete("{id}")]
        public int Delete(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Group.Delete(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }
    }
}

