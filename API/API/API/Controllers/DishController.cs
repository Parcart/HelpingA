﻿using Microsoft.AspNetCore.Mvc;
using Abstraction.Entity;
using System.Collections.Generic;
using API.Data;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DishController : ControllerBase
    {
        // GET: api/<DishController>
        [HttpGet]
        public IEnumerable<IDish> Get()
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Dish.get();
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // GET api/<DishController>/5
        [HttpGet("{id}")]
        public IDish Get(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Dish.get(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // POST api/<DishController>
        [HttpPost]
        public int Post([FromBody] Dish value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Dish.Post(value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // PUT api/<DishController>/5
        [HttpPut("{id}")]
        public int Put(int id, [FromBody] Dish value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Dish.Put(id, value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // DELETE api/<DishController>/5
        [HttpDelete("{id}")]
        public int Delete(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Dish.Delete(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }
    }
}

