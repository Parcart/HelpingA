﻿using Abstraction.Database;
using System;
using System.Collections.Generic;
using System.Text;



namespace Abstraction
{
    public interface IDateBase
    {
        public IDateBaseClient Client { get; }
        public IDateBaseDish Dish { get; }
        public IDateBaseGroup Group { get; }
        public IDateBaseHall Hall { get; }
        public IDateBaseOrder Order { get; }
        public IDateBaseOrder_dish Order_dish { get; }
        public IDateBaseOrder_places order_places { get; }
        public IDateBasePlaces places { get; }
        public IDateBaseUsers Users { get; }
        public IDateBaseWaiters Waiters { get; }
    }
}
