﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Database
{
    public interface IDateBaseReadAll<T>
    {
        public List<T> get();

    }
}
