﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Database
{
    public interface IDateBaseWrite<T>
    {
        public int Post(T value);
        public int Put(int id, T value);
        public int Delete(int id);
        public T CreateOne();
    }
}
