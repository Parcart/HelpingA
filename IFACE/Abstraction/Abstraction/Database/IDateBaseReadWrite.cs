﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Database
{
 public interface IDateBaseReadWrite<T>: 
        IDateBaseReadAll<T>,
        IDateBaseReadOne<T>,
        IDateBaseWrite<T>
    {

    }
}
