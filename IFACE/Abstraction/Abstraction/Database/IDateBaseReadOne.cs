﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Database
{
    public interface IDateBaseReadOne<T>
    {
        public T get(int id );

    }
}
