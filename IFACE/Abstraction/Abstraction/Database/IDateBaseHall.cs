﻿using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Database
{
    public interface IDateBaseHall : IDateBaseReadWrite<Entity.IHall>
    {
        IHall CreateItem();
    }
}
