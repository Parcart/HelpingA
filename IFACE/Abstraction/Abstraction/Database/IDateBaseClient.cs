﻿using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Database
{
    public interface IDateBaseClient : IDateBaseReadWrite<Entity.IClient>

    {
        IClient CreateItem();
    }
}
