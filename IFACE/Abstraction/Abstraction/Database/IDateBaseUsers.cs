﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Database
{
    public interface IDateBaseUsers : IDateBaseReadWrite<Entity.Users>
    {
        public Entity.Users GetByUserName(string userName);
    }
}
