﻿using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity

{
    public class DishRest
    {
        public long Dish_Id { get; set; }
        public string Group_id { get; set; }
        public string Name { get; set; }
        public long Price { get; set; }
    }
}
