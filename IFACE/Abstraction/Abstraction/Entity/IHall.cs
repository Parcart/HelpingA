﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
    public interface IHall
    {
        public long Hall_id { get; set; }
        public string Name { get; set; }
    }
}
