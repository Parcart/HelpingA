﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
    public interface IWaiters
    {
        public long Waiter_id { get; set; }
        public string FIO { get; set; }
        public DateTime HappyB { get; set; }
        public string TelephoneNumber { get; set; }

    }
}
