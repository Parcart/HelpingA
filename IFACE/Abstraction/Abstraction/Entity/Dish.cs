﻿using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity

{
    public class Dish : IDish
    {
        public const string OP = "Dish_ID";
        public long Dish_Id { get; set; }
        public long Group_id { get; set; }
        public string Name { get; set; }
        public Double Price { get; set; }

        public Dish()
        {
        Name = "";
        }
    }
}
