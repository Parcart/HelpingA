﻿using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
    public class Client : IClient
    {
        public const string OP = "Client_ID";
        public long Client_id { get; set; }
        public string FIO { get; set; }
        public string TelephoneNumber { get; set; }
        public DateTime HappyB { get; set; }
        public long Discount { get; set; }

        public Client() 
        {
            FIO = "";
        }
    }
}
