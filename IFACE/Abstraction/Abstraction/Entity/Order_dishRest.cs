﻿using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity

{
    public class Order_dishRest
    {
        public const string OP = "Order_dish_ID";
        public long id { get; set; }
        public long Order_id { get; set; }
        public long Dish_id { get; set; }
        public string Dish_name { get; set; }
        public float Amount { get; set; }
        public double price { get; set; }
        public double sumpa { get; set; }
    }
}
