﻿using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity

{
    public class Reservation : IReservation
    {
        public long reservation_id { get; set; }
        public DateTime Data { get; set; }
        public string Fio { get; set; }
        public long Order_Id { get; set; }

        public Reservation()
        {
            Fio = "";
        }
    }
}
