﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
    public interface IGroup
    {
        public long Group_id { get; set; }
        public string Name { get; set; }
    }
}
