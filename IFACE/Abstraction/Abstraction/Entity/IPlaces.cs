﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
    public interface IPlaces
    {
        public long Places_id { get; set; }
        public string stolik { get; set; }
        public long Hall_id { get; set; }

    }
}
