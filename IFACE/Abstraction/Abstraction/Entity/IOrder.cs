﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
    public interface IOrder
    {
        public long Order_id { get; set; }
        public long? client_id { get; set; }
        public string clientName { get; set; }
        public DateTime Data { get; set; }
        public long Waiter_ID { get; set; }
        public double TotalPrice { get; set; }
        public string complite { get; set; }
        public string IsReservation { get; set; }
        public DateTime? DateReservation { get; set; }
        public string OrderReservStart { get; set; }

    }
}
