﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
    public interface IOrder_places
    {
        public long id { get; set; }
        public long Order_id { get; set; }
        public long Places_id { get; set; }

    }
}
