﻿using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity

{
    public class OrderRest
    {
        public long Order_id { get; set; }
        public string clientName { get; set; }
        public DateTime Data { get; set; }
        public string Waiters_id { get; set; }
        public double TotalPrice { get; set; }
        public string complite { get; set; }
        public string IsReservation { get; set; }
        public DateTime? DateReservation { get; set; }
    }
}
