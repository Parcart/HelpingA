﻿using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity

{
    public class Hall : IHall
    {
        public const string OP = "Hall_ID";
        public long Hall_id { get; set; }
        public string Name { get; set; }

        public Hall()
        {
            Name = "";
        }
    }
}
