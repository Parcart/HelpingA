﻿using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity

{
    public class PlacesRest
    {
        public long Places_id { get; set; }
        public long Hall_idid { get; set; }
        public string Hall_id { get; set; }
        public string Table { get; set; }
        public long id { get; set; }
    }
}
