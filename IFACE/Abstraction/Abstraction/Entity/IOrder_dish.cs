﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
    public interface IOrder_dish
    {
        public long id { get; set; }
        public long Order_id { get; set; }
        public long Dish_id { get; set; }
        public float Amount { get; set; }
        public double price { get; set; }
    }
}

