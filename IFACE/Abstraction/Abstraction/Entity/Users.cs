﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace Abstraction.Entity
{
    public class Users
    {
        private const string Salt = "id301kis";
        public long Users_ID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool ValidatePassword(string pass)
        {
            bool result = false;
            MD5 alg = MD5.Create();
            byte[] passArr = Utils.StringToByteUtils.StringToByteArr(pass + Salt);
            alg.Initialize();
            byte[] data = alg.ComputeHash(passArr);
            string passEncrypted = Utils.StringToByteUtils.ByteArrToHashString(data);
            result = (passEncrypted == Password);
            return result;
        }
    }
}
