﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
    public interface IDish
    {
        public long Dish_Id { get; set; }
        public string Name { get; set; }
        public Double Price { get; set; }
        public long Group_id { get; set; }

    }
}
