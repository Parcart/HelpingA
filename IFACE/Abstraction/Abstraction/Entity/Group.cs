﻿using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity

{
    public class Group : IGroup
    {
        public const string OP = "Group_ID";
        public long Group_id { get; set; }
        public string Name { get; set; }
        public Group()
        {
            Name = "";
        }
    }
}
