﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
   public interface IClient
    {
        public long Client_id { get; set; }
        public string FIO { get; set; }
        public string TelephoneNumber { get; set; }
        public DateTime HappyB { get; set; }
        public long Discount { get; set; }

    }
}
