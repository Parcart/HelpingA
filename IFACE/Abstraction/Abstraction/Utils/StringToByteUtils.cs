﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Utils
{
    public class StringToByteUtils
    {
        public static string ByteToString(byte b)
        {
            string result = $"{(char)b}";
            return result;
        }

        public static string ByteArrToString(byte[] b)
        {
            string result = "";
            for (int i = 0; i < b.Length; i++)
            {
                result += ByteToString(b[i]);
            }
            return result;
        }
        public static string ByteArrToHashString(byte[] b)
        {
            string result = "";
            for (int i = 0; i < b.Length; i++)
            {
                result += b[i].ToString("X2");
            }
            return result;
        }
        public static byte[] StringToByteArr(string s)
        {
            byte[] arr = new byte[s.Length];
            for (int i = 0; i < s.Length; i++)
            {
                arr[i] = (byte)s[i];
            }
            return arr;
        }
    }
}

