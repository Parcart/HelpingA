﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction
{
    public interface IDatabaseSettings
    {
        public string Location { get; set; }
        public string Database { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

    }
}