﻿
using Abstraction.Entity;

namespace REST.Entity
{
    public partial class REST
    {
        public class APIOrder_places : ClassReadWrite<Order_places>
        {
            protected override string _BASE_URI => "/api/Order_places";
            protected override RestAPI _instance =>  RestAPI.Instance;

        }
    }
}
