﻿
using Abstraction.Entity;

namespace REST.Entity
{
    public partial class REST
    {
        public class APIPlaces : ClassReadWrite<Places>
        {
            protected override string _BASE_URI => "/api/Places";
            protected override RestAPI _instance =>  RestAPI.Instance;

        }
    }
}
