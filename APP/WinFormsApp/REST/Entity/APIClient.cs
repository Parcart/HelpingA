﻿
using Abstraction.Entity;

namespace REST.Entity
{
    public partial class REST
    {
        public class APIClient : ClassReadWrite<Client>
        {
            protected override string _BASE_URI => "/api/Client";
            protected override RestAPI _instance =>  RestAPI.Instance;

        }
    }
}
