﻿namespace REST.Entity
{
    public sealed class Result
    {
        public bool Successful { get; set; }
        public object Data { get; set; }
        public Result()
        {
            Successful = false;
            Data = null;
        }
    }
}
