﻿
using Abstraction.Entity;

namespace REST.Entity
{
    public partial class REST
    {
        public class APIOrder : ClassReadWrite<Order>
        {
            protected override string _BASE_URI => "/api/Order";
            protected override RestAPI _instance =>  RestAPI.Instance;

        }
    }
}
