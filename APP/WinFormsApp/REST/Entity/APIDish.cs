﻿
using Abstraction.Entity;

namespace REST.Entity
{
    public partial class API
    {
        public class APIDish : ClassReadWrite<Dish>
        {
            protected override string _BASE_URI => "/api/Dish";
            protected override RestAPI _instance =>  RestAPI.Instance;

        }
    }
}
