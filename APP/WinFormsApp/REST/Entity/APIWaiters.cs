﻿
using Abstraction.Entity;

namespace REST.Entity
{
    public partial class REST
    {
        public class APIWaiters : ClassReadWrite<Waiters>
        {
            protected override string _BASE_URI => "/api/Waiters";
            protected override RestAPI _instance =>  RestAPI.Instance;

        }
    }
}
