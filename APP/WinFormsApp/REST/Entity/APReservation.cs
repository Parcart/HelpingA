﻿
using Abstraction.Entity;

namespace REST.Entity
{
    public partial class REST
    {
        public class APIReservation : ClassReadWrite<Reservation>
        {
            protected override string _BASE_URI => "/api/Reservation";
            protected override RestAPI _instance =>  RestAPI.Instance;

        }
    }
}
