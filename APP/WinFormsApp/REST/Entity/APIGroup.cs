﻿
using Abstraction.Entity;

namespace REST.Entity
{
    public partial class REST
    {
        public class APIGroup : ClassReadWrite<Group>
        {
            protected override string _BASE_URI => "/api/Group";
            protected override RestAPI _instance =>  RestAPI.Instance;

        }
    }
}
