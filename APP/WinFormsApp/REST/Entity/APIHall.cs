﻿
using Abstraction.Entity;

namespace REST.Entity
{
    public partial class REST
    {
        public class APIHall : ClassReadWrite<Hall>
        {
            protected override string _BASE_URI => "/api/Hall";
            protected override RestAPI _instance =>  RestAPI.Instance;

        }
    }
}
