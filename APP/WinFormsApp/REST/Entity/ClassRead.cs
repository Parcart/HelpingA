﻿using REST.Abstraction;
using System;
using System.Collections.Generic;
using System.Text;
using WinFormsApp.REST;

namespace REST.Entity
{
    public class ClassRead<T> : IClassRead<T> where T : new()
    {
        protected virtual string _BASE_URI { get; }
        protected virtual RestAPI _instance { get; }
        public Result Get()
        {
            return _instance.APIGet<List<T>>(_BASE_URI);
        }

        public Result Get(long id)
        {
            return _instance.APIGet<T>($"{_BASE_URI}/{id}");
        }

        public Result Get(long id, long id2)
        {
            return _instance.APIGet<T>($"{_BASE_URI}/{id}!{id2}");
        }
    }
}
