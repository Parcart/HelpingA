﻿
using Abstraction.Entity;

namespace REST.Entity
{
    public partial class REST
    {
        public class APIOrder_dish : ClassReadWrite<Order_dish>
        {
            protected override string _BASE_URI => "/api/Order_dish";
            protected override RestAPI _instance =>  RestAPI.Instance;

        }
    }
}
