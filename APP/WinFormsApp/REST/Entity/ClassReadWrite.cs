﻿using System;
using System.Collections.Generic;
using System.Drawing.Text;
using System.Text;
using REST.Abstraction;
using REST.Entity;

namespace REST.Entity
{
    public class ClassReadWrite<T> : ClassRead<T>, IClassWrite<T> where T : new()
    {
        public Result Delete(long Id)
        {
            string url = $"{_BASE_URI}/{Id}";
            return _instance.APIDelete(url);
        }

        public Result Post(string json)
        {
            return _instance.APIPost<T>(_BASE_URI, json);
        }

        public Result Put(long Id, string json)
        {
            return _instance.APIPut<T>($"{_BASE_URI}/{Id}", json);
        }
    }
}
