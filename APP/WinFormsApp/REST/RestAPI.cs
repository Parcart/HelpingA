﻿using REST.Entity;
using Microsoft.Win32;
using RestSharp;
using System;
using System.Text.Json;
using System.Windows.Forms;
using static REST.Entity.REST;
using WinFormsApp.REST;
using Abstraction.Entity;
using static REST.Entity.API;

namespace REST
{
    public partial class RestAPI
    {
        

        private static RestAPI instance = new RestAPI();
        private static string BaseUrl;
        private string User = null;
        private string Token = null;
        private DateTime TokenValid = DateTime.MinValue;
        public static RestAPI Instance { get { return instance; } }

        private static string GetUrlAPI()
        {
            RegistryKey currentUserKey = Registry.CurrentUser;
            RegistryKey directKey = currentUserKey.OpenSubKey(@"SoftWare\ID301KIS\Stabzzz");
#nullable enable
            string? urlAPIkey = null;
            try
            {
                if(directKey!= null)
                {
                    urlAPIkey = (string?)directKey.GetValue("API-Connect");
                }
            }
            catch
            {
                ;
            }
            if (urlAPIkey != null)
            {
                BaseUrl = urlAPIkey;
            }
            else
            {
                BaseUrl = "http://localhost:5000";
            }
            return BaseUrl;
        }
        RestClient client = new RestClient(GetUrlAPI());
        


        public Result Logining(string user, string password)
        {
            Result result = new Result();
            var request = new RestRequest($"{BaseUrl}/connect/token");
            request.AddParameter("Accept", "application/json", ParameterType.HttpHeader);
            request.AddParameter("Content", "application/x-www-form-urlencoded", ParameterType.HttpHeader);
            request.AddParameter("client_id", "Bruh", ParameterType.GetOrPost);
            request.AddParameter("client_secret", "NoBruh", ParameterType.GetOrPost);
            request.AddParameter("grant_type", "password", ParameterType.GetOrPost);
            request.AddParameter("username", user, ParameterType.GetOrPost);
            request.AddParameter("password", password, ParameterType.GetOrPost);
            var response = client.Execute(request, Method.Post);
            if (response.IsSuccessful)
            {
                AuthenticationResponse token = ParseAuthenticateResponse(response.Content);
                result.Successful = token.expires_in != int.MinValue;
                if (result.Successful)
                {
                    User = user;
                    Token = token.access_token;
                    TokenValid = DateTime.Now.AddSeconds(token.expires_in);
                }
            }
            result.Data = (int)response.StatusCode;
            return result;
        }
        private AuthenticationResponse ParseAuthenticateResponse(string data)
        {
            AuthenticationResponse result;
            try
            {
                result = JsonSerializer.Deserialize<AuthenticationResponse>(data);
            }
            catch
            {
                result = new AuthenticationResponse();
                result.expires_in = int.MinValue;
            }
            return result;
        }
        public bool IsAuthenticated
        {
            get
            {
                return (Token != null && DateTime.Now < TokenValid);
            }
        }

        internal Result APIGet<T>(string url)
        {
            Result result = new Result();
            try
            {
                if (Instance.IsAuthenticated)
                {
                    RestRequest request = new RestRequest(url);
                    var response = client.Get(request);
                    if (response.IsSuccessful) 
                    {
                    result.Data = response.Content;
                    result.Successful = true;
                    }
                    else
                    {
                        result.Data = response.Content;
                        result.Successful = false;
                    }
                    return result;
                }
                else
                {
                    MessageBox.Show("Ваша сессия истекла", "Ошибка");
                    Application.Exit();
                }
                return result;
            }
            catch
            {
                MessageBox.Show("При обращении к серверу произошла ошибка", "Ошибка");
                
                result.Successful = false;
                return result;
            }
        }
        internal Result APIDelete(string url)
        {
            Result result = new Result();
            try
            {
                if (Instance.IsAuthenticated)
                {
                    Method method = Method.Delete;
                    var request = new RestRequest(url, method);
                    var response = client.Execute(request);
                    if (response.IsSuccessful)
                    {
                        result.Data = response.Content;
                        result.Successful = true;
                    }
                    result.Data = response.Content;
                    return result;
                }
                else
                {
                    MessageBox.Show("Ваша сессия истекла", "Ошибка");
                    Application.Exit();

                }
                return result;
            }
            catch
            {
                MessageBox.Show("При обращении к серверу произошла ошибка", "Ошибка");
                result.Successful = false;
                return result;
            }
        }
        internal Result APIPost<T>(string url, string json)
        {
            Result result = new Result();
            try
            {
                if (Instance.IsAuthenticated)
                {
                    Method method = Method.Post;
                    var request = new RestRequest(url, method);
                    request.AddJsonBody(json);
                    var response = client.Execute(request);
                    if (response.IsSuccessful)
                    {
                        result.Successful = true;
                    }
                else
                {
                    result.Successful = false;
                }
                    result.Data = response.Content;
                    return result;
                }
                else
                {
                    MessageBox.Show("Ваша сессия истекла", "Ошибка");
                    Application.Exit();
                }
                return result;
            }
            catch
            {
                MessageBox.Show("При обращении к серверу произошла ошибка", "Ошибка");
                result.Successful = false;
                return result;
            }
        }
        internal Result APIPut<T>(string url, string json)
        {
            Result result = new Result();
            try
            {
                if (Instance.IsAuthenticated)
                {
                    Method method = Method.Put;
                    var request = new RestRequest(url, method);
                    request.AddJsonBody(json);
                    var response = client.Execute(request);
                if (response.IsSuccessful)
                {
                    result.Successful = true;
                }
                else
                {
                    result.Successful = false;
                }
                result.Data = response.Content;
                    return result;
                }
                else
                {
                    MessageBox.Show("Ваша сессия истекла", "Ошибка");
                    Application.Exit();
                }
                return result;
            }
            catch
            {
                MessageBox.Show("При обращении к серверу произошла ошибка", "Ошибка");
                result.Successful = false;
                return result;
            }
        }
        public RestAPI()
        {
            Client = new APIClient();
            Group = new APIGroup();
            Hall = new APIHall();
            Order = new APIOrder();
            Order_dish = new APIOrder_dish();
            Places = new APIPlaces();
            Waiters = new APIWaiters();
            Reservation = new APIReservation();
            Dish = new APIDish();
            Order_places = new APIOrder_places();
        }
       
        public APIClient Client { get; }
        public APIGroup Group { get; }
        public APIHall Hall { get; }
        public APIOrder Order { get; }
        public APIOrder_dish Order_dish { get; }
        public APIPlaces Places { get; }
        public APIWaiters Waiters { get; }
        public APIReservation Reservation { get; }
        public APIDish Dish { get; }
        public APIOrder_places Order_places { get; }

    }
}
