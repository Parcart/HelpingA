﻿using System;
using System.Collections.Generic;
using System.Text;
using REST.Entity;

namespace REST.Abstraction
{
    internal interface IClassWrite<T>
    {
        abstract public Result Post(string json);
        abstract public Result Put(long Id, string json);
        abstract public Result Delete(long Id);
    }
}
