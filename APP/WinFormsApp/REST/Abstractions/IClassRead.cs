﻿using System;
using System.Collections.Generic;
using System.Text;
using REST.Entity;

namespace REST.Abstraction
{
    internal interface IClassRead<T>
    {
        public abstract Result Get();
        public abstract Result Get(long id);
        public abstract Result Get(long id, long id2);
    }
}
