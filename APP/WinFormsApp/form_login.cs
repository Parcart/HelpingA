﻿
using REST;
using REST.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp
{
    public partial class form_login : Form
    {
        public form_login()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            

        }

        private async void btn_login_Click(object sender, EventArgs e)
        {
            if (tb_login.Text !=""& tb_password.Text != "") {
                SwitchEnabled(false);
                Result result = await Task.Run(() => RestAPI.Instance.Logining(tb_login.Text.ToLower(), tb_password.Text));
                if (result.Successful)
                {
                    this.DialogResult = DialogResult.OK;

                }
                else
                {
                    int status = 0;
                    int.TryParse(result.Data.ToString(), out status);
                    string message = status switch
                    {
                        0 => "Нет соединения с сервером!",
                        400 => "Логин или пароль не верны!",
                        _ => "Unknown Error!"
                    };
                    MessageBox.Show(message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                SwitchEnabled(true); 
            }
            else
            {
                MessageBox.Show("Параметры для авторизации не заполненны!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void SwitchEnabled(bool enabled)
        {
            tb_login.Enabled = enabled;
            tb_password.Enabled = enabled;
            btn_login.Enabled = enabled;
        }

        private void frm_login_Load(object sender, EventArgs e)
        {

        }
    }
}
