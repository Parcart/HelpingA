using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new frm_main());
            form_login _login = new form_login();
            DialogResult result = _login.ShowDialog();
            switch (result)
            {
                case DialogResult.OK:
                    Application.Run(new form_main());
                    break;
                default:
                    Application.Exit();
                    break;
            }
        }
    }
}
