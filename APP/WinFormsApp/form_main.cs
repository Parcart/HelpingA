﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using REST;
using REST.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormsApp.SecondFroms.PostPut.OrderAll;

namespace WinFormsApp
{
    public partial class form_main : Form
    {
        public form_main()
        {
            InitializeComponent();
        }
        private void залToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SecondFroms.Get.HallandPlace hallandPlace = new SecondFroms.Get.HallandPlace();
            hallandPlace.Show();
        }

        private void менюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SecondFroms.Get.GroupAndDish groupAndDish = new SecondFroms.Get.GroupAndDish();
            groupAndDish.Show();
        }

        private void клиентыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SecondFroms.Get.ClientW clientW = new SecondFroms.Get.ClientW();
            clientW.Show();
        }

        private void официантыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SecondFroms.Get.WaitersW waitersW = new SecondFroms.Get.WaitersW();
            waitersW.Show();
        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private void заказыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SecondFroms.Get.OrderAll.Order order = new SecondFroms.Get.OrderAll.Order();
            order.Show();
        }

        private void form_main_Load(object sender, EventArgs e)
        {
            loaddb1Async();
            loaddb2Async();
        }
        private async void loaddb1Async()
        {
            Result orderget = await Task.Run(() => RestAPI.Instance.Order.Get());
            List<Abstraction.Entity.Order> order = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Order>>(orderget.Data.ToString());

            Result clientget = await Task.Run(() => RestAPI.Instance.Client.Get());
            List<Abstraction.Entity.Client> client = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Client>>(clientget.Data.ToString());

            Result waitget = await Task.Run(() => RestAPI.Instance.Waiters.Get());
            List<Abstraction.Entity.Waiters> waiters = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Waiters>>(waitget.Data.ToString());

            List<Abstraction.Entity.OrderRest> orderRests = new List<Abstraction.Entity.OrderRest>();

            order.ForEach(cell =>
            {
                if ((cell.IsReservation == "0" && cell.complite == "0") || (cell.IsReservation == "1" && cell.complite == "0" && cell.OrderReservStart == "1"))
                {
                    Abstraction.Entity.OrderRest orderRestsN = new Abstraction.Entity.OrderRest();
                    if (cell.DateReservation != null)
                    {
                        orderRestsN.DateReservation = cell.DateReservation;
                    }
                    orderRestsN.Order_id = cell.Order_id;
                    if (cell.client_id == null)
                    {
                        orderRestsN.clientName = cell.clientName;
                    }
                    else
                    {
                        orderRestsN.clientName = client.Where(cellF => cellF.Client_id == cell.client_id).FirstOrDefault().FIO;
                    }
                    orderRestsN.Data = cell.Data;
                    orderRestsN.Waiters_id = waiters.Where(cellW => cellW.Waiter_id == cell.Waiter_ID).FirstOrDefault().FIO;
                    if (cell.complite == "0")
                    {
                        orderRestsN.complite = "Нет";
                    }
                    else { orderRestsN.complite = "Да"; }
                    if (cell.IsReservation == "0") { orderRestsN.IsReservation = "Нет"; }
                    else { orderRestsN.IsReservation = "Да"; }
                    orderRestsN.TotalPrice = cell.TotalPrice;
                    orderRests.Add(orderRestsN);
                }
            });


            dataGridView1.DataSource = orderRests;
            dataGridView1.Columns[0].HeaderText = "Номер заказа";
            dataGridView1.Columns[1].HeaderText = "Клиент";
            dataGridView1.Columns[2].HeaderText = "Дата";
            dataGridView1.Columns[3].HeaderText = "Официант";
            dataGridView1.Columns[4].Visible = false;
            dataGridView1.Columns[5].Visible = false;
            dataGridView1.Columns[6].Visible = false;
            dataGridView1.Columns[7].Visible = false;
        }
        private async void loaddb2Async()
        {
            Result orderget = await Task.Run(() => RestAPI.Instance.Order.Get());
            List<Abstraction.Entity.Order> order = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Order>>(orderget.Data.ToString());

            Result clientget = await Task.Run(() => RestAPI.Instance.Client.Get());
            List<Abstraction.Entity.Client> client = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Client>>(clientget.Data.ToString());

            Result waitget = await Task.Run(() => RestAPI.Instance.Waiters.Get());
            List<Abstraction.Entity.Waiters> waiters = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Waiters>>(waitget.Data.ToString());

            List<Abstraction.Entity.OrderRest> orderRests = new List<Abstraction.Entity.OrderRest>();

            order.ForEach(cell =>
            {
                if (cell.IsReservation == "1" && cell.complite == "0" && cell.OrderReservStart == "0")
                {
                    Abstraction.Entity.OrderRest orderRestsN = new Abstraction.Entity.OrderRest();
                    if (cell.DateReservation != null)
                    {
                        orderRestsN.DateReservation = cell.DateReservation;
                    }
                    orderRestsN.Order_id = cell.Order_id;
                    if (cell.client_id == null)
                    {
                        orderRestsN.clientName = cell.clientName;
                    }
                    else
                    {
                        orderRestsN.clientName = client.Where(cellF => cellF.Client_id == cell.client_id).FirstOrDefault().FIO;
                    }
                    orderRestsN.Data = cell.Data;
                    orderRestsN.Waiters_id = waiters.Where(cellW => cellW.Waiter_id == cell.Waiter_ID).FirstOrDefault().FIO;
                    if (cell.complite == "0")
                    {
                        orderRestsN.complite = "Нет";
                    }
                    else { orderRestsN.complite = "Да"; }
                    if (cell.IsReservation == "0") { orderRestsN.IsReservation = "Нет"; }
                    else { orderRestsN.IsReservation = "Да"; }
                    orderRestsN.TotalPrice = cell.TotalPrice;
                    orderRests.Add(orderRestsN);
                }
            });


            dataGridView2.DataSource = orderRests;
            dataGridView2.Columns[0].HeaderText = "Номер заказа";
            dataGridView2.Columns[1].HeaderText = "Клиент";
            dataGridView2.Columns[2].HeaderText = "Дата";
            dataGridView2.Columns[3].HeaderText = "Официант";
            dataGridView2.Columns[4].Visible = false;
            dataGridView2.Columns[5].Visible = false;
            dataGridView2.Columns[6].Visible = false;
            dataGridView2.Columns[7].HeaderText = "Дата брони";
        }

        private void splitContainer2_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void BT_ADDNEW_Click(object sender, EventArgs e)
        {
            SecondFroms.PostPut.OrderAll.OrderPP hall = new OrderPP();
            hall.SelectMethod = OrderPP.SelectMethodEnum.Post;
            hall.SelectOrder = OrderPP.SelectOrderWEnum.NO;
            hall.ShowDialog();
            loaddb1Async();
            loaddb2Async();
        }

        private void btn_POSTZAK_Click(object sender, EventArgs e)
        {
            if (Resultid(dataGridView1.CurrentRow) != 0)
            {
                OrderPP hall = new OrderPP();
                hall.SelectMethod = OrderPP.SelectMethodEnum.Put;
                if (dataGridView1.CurrentRow.Cells[6].Value.ToString() == "Да")
                {
                    hall.SelectOrder = OrderPP.SelectOrderWEnum.Reserv;
                }
                else
                {
                    hall.SelectOrder = OrderPP.SelectOrderWEnum.NO;
                }
                hall.Selectid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
                hall.ShowDialog();
                loaddb1Async();
                loaddb2Async();
            }
            else
            {
                MessageBox.Show("Строка не выбрана");
            }
        }
        private int Resultid(object? row)
        {
            if (row != null)
            {
                int Selectid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
                return Selectid;
            }
            return Convert.ToInt32(row);
        }
        private int Resultid2(object? row)
        {
            if (row != null)
            {
                int Selectid = Convert.ToInt32(dataGridView2.CurrentRow.Cells[0].Value);
                return Selectid;
            }
            return Convert.ToInt32(row);
        }

        private void btn_CREATERES_Click(object sender, EventArgs e)
        {
            OrderPP hall = new OrderPP();
            hall.SelectMethod = OrderPP.SelectMethodEnum.Post;
            hall.SelectOrder = OrderPP.SelectOrderWEnum.Reserv;
            hall.ShowDialog();
            loaddb1Async();
            loaddb2Async();
        }

        private void btn_Posadka_Click(object sender, EventArgs e)
        {
            if (Resultid2(dataGridView2.CurrentRow) != 0)
            {
                DialogResult msg = MessageBox.Show("Перенос заказа из бронирование в иcполнение!", "Подтверждение", MessageBoxButtons.OKCancel);

                if (msg == DialogResult.OK)
                {
                    Result orderget = RestAPI.Instance.Order.Get(Resultid2(dataGridView2.CurrentRow));
                    Abstraction.Entity.Order order = Newtonsoft.Json.JsonConvert.DeserializeObject<Abstraction.Entity.Order>(orderget.Data.ToString());
                    JObject jsonObject = new JObject();
                    jsonObject.Add("order_id", 0);
                    jsonObject.Add("client_id", order.client_id);
                    jsonObject.Add("clientName", order.clientName);
                    jsonObject.Add("data", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
                    jsonObject.Add("Waiter_ID", order.Waiter_ID);
                    jsonObject.Add("totalPrice", order.TotalPrice);
                    jsonObject.Add("complite", order.complite);
                    jsonObject.Add("isReservation", order.IsReservation);
                    jsonObject.Add("dateReservation", order.DateReservation);
                    jsonObject.Add("OrderReservStart", "1");
                    string json = JsonConvert.SerializeObject(jsonObject);
                    Result result = RestAPI.Instance.Order.Put(Resultid2(dataGridView2.CurrentRow), json);
                    loaddb1Async();
                    loaddb2Async();
                }
            }
            else
            {
                MessageBox.Show("Строка не выбрана");
            }
        }

        private void splitContainer3_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private void btn_UPDATE_Click(object sender, EventArgs e)
        {
            loaddb1Async();
            loaddb2Async();
        }

        private void btn_UPDATEFORDATE_Click(object sender, EventArgs e)
        {
            DateTime date = dateTimePicker1.Value;

            Result orderget = RestAPI.Instance.Order.Get();
            List<Abstraction.Entity.Order> order = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Order>>(orderget.Data.ToString());

            Result clientget = RestAPI.Instance.Client.Get();
            List<Abstraction.Entity.Client> client = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Client>>(clientget.Data.ToString());

            Result waitget = RestAPI.Instance.Waiters.Get();
            List<Abstraction.Entity.Waiters> waiters = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Waiters>>(waitget.Data.ToString());

            List<Abstraction.Entity.OrderRest> orderRests = new List<Abstraction.Entity.OrderRest>();

            order.ForEach(cell =>
            {
                if (((cell.Data.Date == date.Date) && cell.IsReservation == "0" && cell.complite == "0") || ((cell.Data.Date == date.Date) && cell.IsReservation == "1" && cell.complite == "0" && cell.OrderReservStart == "1"))
                {
                    Abstraction.Entity.OrderRest orderRestsN = new Abstraction.Entity.OrderRest();
                    if (cell.DateReservation != null)
                    {
                        orderRestsN.DateReservation = cell.DateReservation;
                    }
                    orderRestsN.Order_id = cell.Order_id;
                    if (cell.client_id == null)
                    {
                        orderRestsN.clientName = cell.clientName;
                    }
                    else
                    {
                        orderRestsN.clientName = client.Where(cellF => cellF.Client_id == cell.client_id).FirstOrDefault().FIO;
                    }
                    orderRestsN.Data = cell.Data;
                    orderRestsN.Waiters_id = waiters.Where(cellW => cellW.Waiter_id == cell.Waiter_ID).FirstOrDefault().FIO;
                    if (cell.complite == "0")
                    {
                        orderRestsN.complite = "Нет";
                    }
                    else { orderRestsN.complite = "Да"; }
                    if (cell.IsReservation == "0") { orderRestsN.IsReservation = "Нет"; }
                    else { orderRestsN.IsReservation = "Да"; }
                    orderRestsN.TotalPrice = cell.TotalPrice;
                    orderRests.Add(orderRestsN);
                }
            });


            dataGridView1.DataSource = orderRests;
            dataGridView1.Columns[0].HeaderText = "Номер заказа";
            dataGridView1.Columns[1].HeaderText = "Клиент";
            dataGridView1.Columns[2].HeaderText = "Дата";
            dataGridView1.Columns[3].HeaderText = "Официант";
            dataGridView1.Columns[4].Visible = false;
            dataGridView1.Columns[5].Visible = false;
            dataGridView1.Columns[6].Visible = false;
            dataGridView1.Columns[7].Visible = false;

            orderRests = new List<Abstraction.Entity.OrderRest>();

            order.ForEach(cell =>
            {
                if (cell.DateReservation.HasValue && cell.IsReservation == "1" && cell.complite == "0" && cell.OrderReservStart == "0")
                {
                    if (cell.DateReservation.Value.Date == date.Date)
                    {
                        Abstraction.Entity.OrderRest orderRestsN = new Abstraction.Entity.OrderRest();
                        if (cell.DateReservation != null)
                        {
                            orderRestsN.DateReservation = cell.DateReservation;
                        }
                        orderRestsN.Order_id = cell.Order_id;
                        if (cell.client_id == null)
                        {
                            orderRestsN.clientName = cell.clientName;
                        }
                        else
                        {
                            orderRestsN.clientName = client.Where(cellF => cellF.Client_id == cell.client_id).FirstOrDefault().FIO;
                        }
                        orderRestsN.Data = cell.Data;
                        orderRestsN.Waiters_id = waiters.Where(cellW => cellW.Waiter_id == cell.Waiter_ID).FirstOrDefault().FIO;
                        if (cell.complite == "0")
                        {
                            orderRestsN.complite = "Нет";
                        }
                        else { orderRestsN.complite = "Да"; }
                        if (cell.IsReservation == "0") { orderRestsN.IsReservation = "Нет"; }
                        else { orderRestsN.IsReservation = "Да"; }
                        orderRestsN.TotalPrice = cell.TotalPrice;
                        orderRests.Add(orderRestsN);
                    }
                }
            });


            dataGridView2.DataSource = orderRests;
            dataGridView2.Columns[0].HeaderText = "Номер заказа";
            dataGridView2.Columns[1].HeaderText = "Клиент";
            dataGridView2.Columns[2].HeaderText = "Дата";
            dataGridView2.Columns[3].HeaderText = "Официант";
            dataGridView2.Columns[4].Visible = false;
            dataGridView2.Columns[5].Visible = false;
            dataGridView2.Columns[6].Visible = false;
            dataGridView2.Columns[7].HeaderText = "Дата брони";
        }

        private void BTN_PUT2_Click(object sender, EventArgs e)
        {
            if (Resultid2(dataGridView2.CurrentRow) != 0)
            {
                OrderPP hall = new OrderPP();
                hall.SelectMethod = OrderPP.SelectMethodEnum.Put;
                if (dataGridView2.CurrentRow.Cells[6].Value.ToString() == "Да")
                {
                    hall.SelectOrder = OrderPP.SelectOrderWEnum.Reserv;
                }
                else
                {
                    hall.SelectOrder = OrderPP.SelectOrderWEnum.NO;
                }
                hall.Selectid = Convert.ToInt32(dataGridView2.CurrentRow.Cells[0].Value);
                hall.ShowDialog();
                loaddb1Async();
                loaddb2Async();
            }
            else
            {
                MessageBox.Show("Строка не выбрана");
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
