﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using REST.Entity;
using REST;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp.SecondFroms.PostPut
{
    public partial class WaitersPP : Form
    {
        public WaitersPP()
        {
            InitializeComponent();
        }

        public enum SelectMethodEnum
        {
            Put,
            Post,
        }
        private SelectMethodEnum _method;
        public virtual SelectMethodEnum SelectMethod { get { return _method; } set { _method = value; } }
        private int? _selectid;
        public virtual int? Selectid { get { return _selectid; } set { _selectid = value; } }
        private void AsyncMessageBox(Result result)
        {
            if (SelectMethod == SelectMethodEnum.Post)
            {

            }
            else if (SelectMethod == SelectMethodEnum.Put & result.Data.ToString() != "0")
            {

            }
            else
            {
                MessageBox.Show("Ошибка", "Ошибка");
            }
        }
        private async void btn_dobav_ClickAsync(object sender, EventArgs e)
        {
            if (tb_telephone.MaskFull & (SelectMethod == SelectMethodEnum.Post && (tb_FIO.Text != "")) | (SelectMethod == SelectMethodEnum.Put && tb_telephone.Text != "" && tb_FIO.Text != ""))
            {
                JObject jsonObject = new JObject();
                jsonObject.Add("waiter_id", 0);
                jsonObject.Add("fio", tb_FIO.Text);
                jsonObject.Add("telephoneNumber", tb_telephone.Text);
                jsonObject.Add("happyB", tb_date.Value.ToString("yyyy-MM-dd"));
                string json = JsonConvert.SerializeObject(jsonObject);
                if (SelectMethod == SelectMethodEnum.Put)
                {
                    Result result = await Task.Run(() => RestAPI.Instance.Waiters.Put((long)Selectid, json));
                    AsyncMessageBox(result);
                    Close();
                }
                else if (SelectMethod == SelectMethodEnum.Post)
                {
                    Result result = await Task.Run(() => RestAPI.Instance.Waiters.Post(json));
                    AsyncMessageBox(result);
                    Close();
                }
            }
            else
            {
                MessageBox.Show("Не все данные заполнены");
            }
        }

        private void Hall_Load(object sender, EventArgs e)
        {
            if (SelectMethod == SelectMethodEnum.Post)
            {
                Text = "Добавить официанта";
            }
            else if (SelectMethod == SelectMethodEnum.Put)
            {
                //textBox1.Text = Selectid.ToString();
                Result result = RestAPI.Instance.Waiters.Get((long)Selectid);
                Abstraction.Entity.Waiters hall = Newtonsoft.Json.JsonConvert.DeserializeObject<Abstraction.Entity.Waiters>(result.Data.ToString());
                tb_FIO.Text = hall.FIO;
                tb_telephone.Text = hall.TelephoneNumber;
                Text = "Редактирование официанта";
                btn_dobav.Text = "Вставить";
            }
            else
            {
                Text = "Ошибка в создании формы";
            }
        }

        private void tb_date_ValueChanged(object sender, EventArgs e)
        {
            if (tb_date.Value > DateTime.Today)
            {
                tb_date.Value = DateTime.Today;
                MessageBox.Show("Дата больше текущей!");
            }
        }

        private void tb_FIO_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsControl(e.KeyChar) || char.IsLetter(e.KeyChar))
            {
                return;
            }
            e.Handled = true;
        }
    }
}
