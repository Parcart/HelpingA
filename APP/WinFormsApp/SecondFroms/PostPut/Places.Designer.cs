﻿namespace WinFormsApp.SecondFroms.PostPut
{
    partial class Places
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_name = new System.Windows.Forms.TextBox();
            this.btn_dobav = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cb_hall = new System.Windows.Forms.ComboBox();
            this.bt_dobavhall = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tb_name
            // 
            this.tb_name.Location = new System.Drawing.Point(58, 79);
            this.tb_name.MaxLength = 5;
            this.tb_name.Name = "tb_name";
            this.tb_name.Size = new System.Drawing.Size(165, 23);
            this.tb_name.TabIndex = 0;
            // 
            // btn_dobav
            // 
            this.btn_dobav.Location = new System.Drawing.Point(80, 108);
            this.btn_dobav.Name = "btn_dobav";
            this.btn_dobav.Size = new System.Drawing.Size(110, 53);
            this.btn_dobav.TabIndex = 1;
            this.btn_dobav.Text = "Добавить";
            this.btn_dobav.UseVisualStyleBackColor = true;
            this.btn_dobav.Click += new System.EventHandler(this.btn_dobav_ClickAsync);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(97, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 21);
            this.label1.TabIndex = 2;
            this.label1.Text = "Столик";
            // 
            // cb_hall
            // 
            this.cb_hall.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_hall.FormattingEnabled = true;
            this.cb_hall.Location = new System.Drawing.Point(58, 50);
            this.cb_hall.Name = "cb_hall";
            this.cb_hall.Size = new System.Drawing.Size(165, 23);
            this.cb_hall.TabIndex = 3;
            // 
            // bt_dobavhall
            // 
            this.bt_dobavhall.Location = new System.Drawing.Point(229, 49);
            this.bt_dobavhall.Name = "bt_dobavhall";
            this.bt_dobavhall.Size = new System.Drawing.Size(27, 23);
            this.bt_dobavhall.TabIndex = 4;
            this.bt_dobavhall.Text = "+";
            this.bt_dobavhall.UseVisualStyleBackColor = true;
            this.bt_dobavhall.Click += new System.EventHandler(this.bt_dobavhall_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Зал";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Столик";
            // 
            // Places
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(268, 167);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bt_dobavhall);
            this.Controls.Add(this.cb_hall);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_dobav);
            this.Controls.Add(this.tb_name);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Places";
            this.Text = "Создание залла";
            this.Load += new System.EventHandler(this.Places_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_name;
        private System.Windows.Forms.Button btn_dobav;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cb_hall;
        private System.Windows.Forms.Button bt_dobavhall;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}