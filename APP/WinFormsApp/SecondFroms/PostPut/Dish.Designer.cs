﻿namespace WinFormsApp.SecondFroms.PostPut
{
    partial class Dish
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_name = new System.Windows.Forms.TextBox();
            this.btn_dobav = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cb_Group = new System.Windows.Forms.ComboBox();
            this.bt_dobavhall = new System.Windows.Forms.Button();
            this.tb_price = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tb_name
            // 
            this.tb_name.Location = new System.Drawing.Point(71, 70);
            this.tb_name.MaxLength = 32;
            this.tb_name.Name = "tb_name";
            this.tb_name.Size = new System.Drawing.Size(165, 23);
            this.tb_name.TabIndex = 0;
            this.tb_name.TextChanged += new System.EventHandler(this.tb_name_TextChanged);
            // 
            // btn_dobav
            // 
            this.btn_dobav.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_dobav.Location = new System.Drawing.Point(82, 128);
            this.btn_dobav.Name = "btn_dobav";
            this.btn_dobav.Size = new System.Drawing.Size(110, 53);
            this.btn_dobav.TabIndex = 1;
            this.btn_dobav.Text = "Добавить";
            this.btn_dobav.UseVisualStyleBackColor = true;
            this.btn_dobav.Click += new System.EventHandler(this.btn_dobav_ClickAsync);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(71, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 21);
            this.label1.TabIndex = 2;
            this.label1.Text = "Позиция меню";
            // 
            // cb_Group
            // 
            this.cb_Group.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_Group.FormattingEnabled = true;
            this.cb_Group.Location = new System.Drawing.Point(71, 41);
            this.cb_Group.Name = "cb_Group";
            this.cb_Group.Size = new System.Drawing.Size(165, 23);
            this.cb_Group.TabIndex = 3;
            // 
            // bt_dobavhall
            // 
            this.bt_dobavhall.Location = new System.Drawing.Point(242, 40);
            this.bt_dobavhall.Name = "bt_dobavhall";
            this.bt_dobavhall.Size = new System.Drawing.Size(29, 23);
            this.bt_dobavhall.TabIndex = 4;
            this.bt_dobavhall.Text = "+";
            this.bt_dobavhall.UseVisualStyleBackColor = true;
            this.bt_dobavhall.Click += new System.EventHandler(this.bt_dobavhall_Click);
            // 
            // tb_price
            // 
            this.tb_price.Location = new System.Drawing.Point(71, 99);
            this.tb_price.MaxLength = 6;
            this.tb_price.Name = "tb_price";
            this.tb_price.Size = new System.Drawing.Size(165, 23);
            this.tb_price.TabIndex = 5;
            this.tb_price.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_price_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 15);
            this.label2.TabIndex = 13;
            this.label2.Text = "Категория";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 15);
            this.label3.TabIndex = 14;
            this.label3.Text = "Блюдо";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 15);
            this.label4.TabIndex = 15;
            this.label4.Text = "Цена";
            // 
            // Dish
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(274, 189);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_price);
            this.Controls.Add(this.bt_dobavhall);
            this.Controls.Add(this.cb_Group);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_dobav);
            this.Controls.Add(this.tb_name);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Dish";
            this.Text = "Создание залла";
            this.Load += new System.EventHandler(this.Places_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_name;
        private System.Windows.Forms.Button btn_dobav;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cb_Group;
        private System.Windows.Forms.Button bt_dobavhall;
        private System.Windows.Forms.TextBox tb_price;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}