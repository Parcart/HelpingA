﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using REST.Entity;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using REST;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace WinFormsApp.SecondFroms.PostPut
{
    public partial class Hall : Form
    {
        public Hall()
        {
            InitializeComponent();
        }
        public enum SelectMethodEnum
        {
            Put,
            Post,
        }
        private SelectMethodEnum _method;
        public virtual SelectMethodEnum SelectMethod { get { return _method; } set { _method = value; } }
        private int? _selectid;
        public virtual int? Selectid { get { return _selectid; } set { _selectid = value; } }
        private string _newid;
        public virtual string Newid { get { return _newid; } set { _newid = value; } }
        private void AsyncMessageBox(Result result)
        {
            if (SelectMethod == SelectMethodEnum.Post)
            {

            }
            else if (SelectMethod == SelectMethodEnum.Put & result.Data.ToString() != "0")
            {

            }
            else
            {
                MessageBox.Show("Ошибка", "Ошибка");
            }
        }
        private string startname;
        private async void btn_dobav_ClickAsync(object sender, EventArgs e)
        {
            if ((SelectMethod == SelectMethodEnum.Post && (tb_name.Text != "")) | (SelectMethod == SelectMethodEnum.Put && tb_name.Text != ""))
            {
                Result Group = RestAPI.Instance.Hall.Get();
                List<Abstraction.Entity.Hall> groups = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Hall>>(Group.Data.ToString());
                int wow = groups.Where(cell => cell.Name == tb_name.Text).Count();
                if (wow == 0 || (SelectMethod == SelectMethodEnum.Put && startname == tb_name.Text))
                {
                    JObject jsonObject = new JObject();
                    jsonObject.Add("Hall_id", 0);
                    jsonObject.Add("Name", tb_name.Text);
                    string json = JsonConvert.SerializeObject(jsonObject);
                    if (SelectMethod == SelectMethodEnum.Put)
                    {
                        Result result = await Task.Run(() => RestAPI.Instance.Hall.Put((long)Selectid, json));
                        AsyncMessageBox(result);
                        Close();
                    }
                    else if (SelectMethod == SelectMethodEnum.Post)
                    {
                        Result result = await Task.Run(() => RestAPI.Instance.Hall.Post(json));
                        if (result.Successful)
                        {
                            Newid = tb_name.Text;
                        }
                        AsyncMessageBox(result);
                        Close();
                    }
                }
                else
                {
                    MessageBox.Show("Зал с таким названием уже существует!");
                }
            }
            else
            {
                MessageBox.Show("Не все данные заполнены!");
            }
        }

        private void Hall_Load(object sender, EventArgs e)
        {
            if (SelectMethod == SelectMethodEnum.Post)
            {
                Text = "Создание зала";
            }
            else if (SelectMethod == SelectMethodEnum.Put)
            {
                //textBox1.Text = Selectid.ToString();
                Result result = RestAPI.Instance.Hall.Get((long)Selectid);
                Abstraction.Entity.Hall hall = Newtonsoft.Json.JsonConvert.DeserializeObject<Abstraction.Entity.Hall>(result.Data.ToString());
                tb_name.Text = hall.Name;
                Text = "Обновление зала";
                btn_dobav.Text = "Вставить";
            }
            else
            {
                Text = "Ошибка в создании формы";
            }
        }
    }
}
