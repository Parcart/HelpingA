﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using REST.Entity;
using REST;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Abstraction.Entity;

namespace WinFormsApp.SecondFroms.PostPut.OrderAll
{
    public partial class DishAdd : Form
    {
        public DishAdd()
        {
            InitializeComponent();
        }
        public enum SelectMethodEnum
        {
            Put,
            Post,
        }
        private SelectMethodEnum _method;
        public virtual SelectMethodEnum SelectMethod { get { return _method; } set { _method = value; } }
        private int? _selectid;
        public virtual int? Selectid { get { return _selectid; } set { _selectid = value; } }
        
        private Abstraction.Entity.Order_dishRest _order_dishRest;
        internal virtual Abstraction.Entity.Order_dishRest Order_dishRest { get { return _order_dishRest; } set { _order_dishRest = value; } }
        private void AsyncMessageBox(Result result)
        {
            if (SelectMethod == SelectMethodEnum.Post)
            {

            }
            else if (SelectMethod == SelectMethodEnum.Put & result.Data.ToString() != "0")
            {

            }
            else
            {
                MessageBox.Show("Ошибка", "Ошибка");
            }
        }

        private void Places_Load(object sender, EventArgs e)
        {
            loadComboBox();
            if (SelectMethod == SelectMethodEnum.Post)
            {
                Text = "Создание записи";
                label1.Visible = false;
            }
            else if (SelectMethod == SelectMethodEnum.Put)
            {
                //textBox1.Text = Selectid.ToString();
                Result result = RestAPI.Instance.Dish.Get((long)Selectid);
                Abstraction.Entity.Dish dish = Newtonsoft.Json.JsonConvert.DeserializeObject<Abstraction.Entity.Dish>(result.Data.ToString());
                lb_price.Text = dish.Price.ToString();
                tb_col.Text = Order_dishRest.Amount.ToString();
                cb_Group.Text = getName(group, dish.Group_id);
                cb_dish.Text = getName(dishslov, dish.Dish_Id);
                Text = "Обновление записи";
                btn_dobav.Text = "Вставить";
            }
            else
            {
                Text = "Ошибка в создании формы";
            }
        }
        private Dictionary<long, string> group = new Dictionary<long, string>();
        private Dictionary<long, string> dishslov = new Dictionary<long, string>();
        private void loadComboBox()
        {
            group.Clear();
            cb_Group.Items.Clear();
            Result Groupget = RestAPI.Instance.Group.Get();
            List<Abstraction.Entity.Group> Group = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Group>>(Groupget.Data.ToString());
            Group.ForEach(cell =>
            {
                group.Add(cell.Group_id, cell.Name);
                cb_Group.Items.Add(cell.Name);
            });
            if (SelectMethod == SelectMethodEnum.Put) 
            {
                dishslov.Clear();
                cb_dish.Items.Clear();
                Result result = RestAPI.Instance.Dish.Get();
                List<Abstraction.Entity.Dish> dishn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Dish>>(result.Data.ToString());
                dishn.ForEach(cell =>
                {
                    dishslov.Add(cell.Dish_Id, cell.Name);
                    cb_dish.Items.Add(cell.Name);
                });
                cb_dish.Text = dishn.Where(cell => cell.Dish_Id == (long)Selectid).FirstOrDefault().Name;
            }
        }
        private long getID(Dictionary<long, string> dict, ComboBox comboBox)
        {
            long id = 0;
            foreach (var name in dict)
            {
                if (name.Value == comboBox.Text)
                {
                    id = name.Key;
                    return id;
                }
            }
            return id;
        }
        private string getName(Dictionary<long, string> dict, long id)
        {
            string name = "";
            foreach (var row in dict)
            {
                if (row.Key == id)
                {
                    name = row.Value;
                    return name;
                }
            }
            return name;
        }

        private async void btn_dobav_ClickAsync(object sender, EventArgs e)
        {
            if (cb_dish.Text != "" && cb_Group.Text != "" && (tb_col.Text != "" & tb_col.Text != "0"))
            {
                Abstraction.Entity.Order_dishRest order_DishRest = new Abstraction.Entity.Order_dishRest();
                order_DishRest.Dish_id = getID(dishslov, cb_dish);
                order_DishRest.price = Convert.ToDouble(lb_price.Text);
                order_DishRest.Dish_name = cb_dish.Text;
                order_DishRest.Amount = Convert.ToInt64(tb_col.Text);
                Order_dishRest = order_DishRest;
                Close();
            }
            else
            {
                MessageBox.Show("Не все данные заполнены");
            }
        }

        private void bt_dobavhall_Click(object sender, EventArgs e)
        {
            Group hall = new Group();
            hall.SelectMethod = Group.SelectMethodEnum.Post;
            hall.ShowDialog();
            loadComboBox();
        }

        private void tb_price_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void cb_Group_SelectedIndexChanged(object sender, EventArgs e)
        {
            dishslov.Clear();
            cb_dish.Items.Clear();
            Result result = RestAPI.Instance.Dish.Get();
            List<Abstraction.Entity.Dish> dishn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Dish>>(result.Data.ToString());
            dishn.Where(cell => cell.Group_id == getID(group, cb_Group)).ToList().ForEach(cell =>
            {
                dishslov.Add(cell.Dish_Id, cell.Name);
                cb_dish.Items.Add(cell.Name);
            });
        }

        private void cb_dish_SelectedIndexChanged(object sender, EventArgs e)
        {
            Result result = RestAPI.Instance.Dish.Get();
            List<Abstraction.Entity.Dish> dishn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Dish>>(result.Data.ToString());
            lb_price.Text = dishn.Where(cell => cell.Dish_Id == getID(dishslov, cb_dish)).FirstOrDefault().Price.ToString();
        }

        private void tb_col_TextChanged(object sender, EventArgs e)
        {
            if(lb_price.Text != "" && tb_col.Text != "") 
            {
                int sum = int.Parse(lb_price.Text) * int.Parse(tb_col.Text);
                lb_sum.Text = sum.ToString();
            }
        }

        private void lb_price_TextAlignChanged(object sender, EventArgs e)
        {
            if (tb_col.Text != "0" && tb_col.Text != "")
            {
                int sum = int.Parse(lb_price.Text) * int.Parse(tb_col.Text);
                lb_sum.Text = sum.ToString();
            }
        }
    }
}
