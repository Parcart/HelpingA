﻿using System.Windows.Forms;

namespace WinFormsApp.SecondFroms.PostPut.OrderAll
{
    partial class OrderPP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tb_clientName = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lb_menu = new System.Windows.Forms.Label();
            this.btn_DELETE = new System.Windows.Forms.Button();
            this.btn_REDACT = new System.Windows.Forms.Button();
            this.btn_ADD = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.btn_placeDELETE = new System.Windows.Forms.Button();
            this.btn_palacePUT = new System.Windows.Forms.Button();
            this.btn_placePOST = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_dateBron = new System.Windows.Forms.DateTimePicker();
            this.btn_CREATEorder = new System.Windows.Forms.Button();
            this.cb_waiters = new System.Windows.Forms.ComboBox();
            this.btn_COMPLITE = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Клиент";
            // 
            // tb_clientName
            // 
            this.tb_clientName.Location = new System.Drawing.Point(75, 58);
            this.tb_clientName.MaxLength = 100;
            this.tb_clientName.Name = "tb_clientName";
            this.tb_clientName.Size = new System.Drawing.Size(144, 23);
            this.tb_clientName.TabIndex = 1;
            this.tb_clientName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_clientName_KeyPress);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(225, 58);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Выбрать клиента";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label12.Location = new System.Drawing.Point(364, 628);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 21);
            this.label12.TabIndex = 48;
            this.label12.Text = "label12";
            this.label12.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label13.Location = new System.Drawing.Point(262, 628);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(96, 21);
            this.label13.TabIndex = 47;
            this.label13.Text = "Количество:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label9.Location = new System.Drawing.Point(204, 628);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 21);
            this.label9.TabIndex = 46;
            this.label9.Text = "label9";
            this.label9.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label11.Location = new System.Drawing.Point(124, 628);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 21);
            this.label11.TabIndex = 45;
            this.label11.Text = "Позиций:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label8.Location = new System.Drawing.Point(507, 628);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 21);
            this.label8.TabIndex = 44;
            this.label8.Text = "label8";
            this.label8.Visible = false;
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label7.Location = new System.Drawing.Point(445, 628);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 21);
            this.label7.TabIndex = 43;
            this.label7.Text = "Итого:";
            // 
            // lb_menu
            // 
            this.lb_menu.AutoSize = true;
            this.lb_menu.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lb_menu.Location = new System.Drawing.Point(5, 264);
            this.lb_menu.Name = "lb_menu";
            this.lb_menu.Size = new System.Drawing.Size(68, 25);
            this.lb_menu.TabIndex = 42;
            this.lb_menu.Text = "Блюда";
            // 
            // btn_DELETE
            // 
            this.btn_DELETE.Location = new System.Drawing.Point(232, 292);
            this.btn_DELETE.Name = "btn_DELETE";
            this.btn_DELETE.Size = new System.Drawing.Size(100, 35);
            this.btn_DELETE.TabIndex = 41;
            this.btn_DELETE.Text = "Удалить";
            this.btn_DELETE.UseVisualStyleBackColor = true;
            this.btn_DELETE.Click += new System.EventHandler(this.btn_DELETE_Click);
            // 
            // btn_REDACT
            // 
            this.btn_REDACT.Location = new System.Drawing.Point(129, 292);
            this.btn_REDACT.Name = "btn_REDACT";
            this.btn_REDACT.Size = new System.Drawing.Size(97, 35);
            this.btn_REDACT.TabIndex = 40;
            this.btn_REDACT.Text = "Изменить";
            this.btn_REDACT.UseVisualStyleBackColor = true;
            this.btn_REDACT.Click += new System.EventHandler(this.btn_REDACT_Click);
            // 
            // btn_ADD
            // 
            this.btn_ADD.Location = new System.Drawing.Point(12, 292);
            this.btn_ADD.Name = "btn_ADD";
            this.btn_ADD.Size = new System.Drawing.Size(111, 35);
            this.btn_ADD.TabIndex = 39;
            this.btn_ADD.Text = "Добавить";
            this.btn_ADD.UseVisualStyleBackColor = true;
            this.btn_ADD.Click += new System.EventHandler(this.btn_ADD_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(5, 333);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(563, 292);
            this.dataGridView1.TabIndex = 38;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(5, 191);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowTemplate.Height = 25;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(563, 70);
            this.dataGridView2.TabIndex = 49;
            // 
            // btn_placeDELETE
            // 
            this.btn_placeDELETE.Location = new System.Drawing.Point(232, 150);
            this.btn_placeDELETE.Name = "btn_placeDELETE";
            this.btn_placeDELETE.Size = new System.Drawing.Size(100, 35);
            this.btn_placeDELETE.TabIndex = 52;
            this.btn_placeDELETE.Text = "Удалить";
            this.btn_placeDELETE.UseVisualStyleBackColor = true;
            this.btn_placeDELETE.Click += new System.EventHandler(this.btn_placeDELETE_Click);
            // 
            // btn_palacePUT
            // 
            this.btn_palacePUT.Location = new System.Drawing.Point(129, 150);
            this.btn_palacePUT.Name = "btn_palacePUT";
            this.btn_palacePUT.Size = new System.Drawing.Size(97, 35);
            this.btn_palacePUT.TabIndex = 51;
            this.btn_palacePUT.Text = "Изменить";
            this.btn_palacePUT.UseVisualStyleBackColor = true;
            this.btn_palacePUT.Click += new System.EventHandler(this.btn_palacePUT_Click);
            // 
            // btn_placePOST
            // 
            this.btn_placePOST.Location = new System.Drawing.Point(12, 150);
            this.btn_placePOST.Name = "btn_placePOST";
            this.btn_placePOST.Size = new System.Drawing.Size(111, 35);
            this.btn_placePOST.TabIndex = 50;
            this.btn_placePOST.Text = "Добавить";
            this.btn_placePOST.UseVisualStyleBackColor = true;
            this.btn_placePOST.Click += new System.EventHandler(this.btn_placePOST_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(5, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 25);
            this.label2.TabIndex = 53;
            this.label2.Text = "Места";
            // 
            // tb_dateBron
            // 
            this.tb_dateBron.Location = new System.Drawing.Point(403, 0);
            this.tb_dateBron.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.tb_dateBron.Name = "tb_dateBron";
            this.tb_dateBron.Size = new System.Drawing.Size(165, 23);
            this.tb_dateBron.TabIndex = 54;
            this.tb_dateBron.Visible = false;
            // 
            // btn_CREATEorder
            // 
            this.btn_CREATEorder.Location = new System.Drawing.Point(-5, 0);
            this.btn_CREATEorder.Name = "btn_CREATEorder";
            this.btn_CREATEorder.Size = new System.Drawing.Size(103, 46);
            this.btn_CREATEorder.TabIndex = 55;
            this.btn_CREATEorder.Text = "Создать заказ";
            this.btn_CREATEorder.UseVisualStyleBackColor = true;
            this.btn_CREATEorder.Click += new System.EventHandler(this.btn_CREATEorder_Click);
            // 
            // cb_waiters
            // 
            this.cb_waiters.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_waiters.FormattingEnabled = true;
            this.cb_waiters.Location = new System.Drawing.Point(75, 96);
            this.cb_waiters.Name = "cb_waiters";
            this.cb_waiters.Size = new System.Drawing.Size(165, 23);
            this.cb_waiters.TabIndex = 56;
            this.cb_waiters.SelectedIndexChanged += new System.EventHandler(this.cb_waiters_SelectedIndexChanged);
            // 
            // btn_COMPLITE
            // 
            this.btn_COMPLITE.Location = new System.Drawing.Point(104, 0);
            this.btn_COMPLITE.Name = "btn_COMPLITE";
            this.btn_COMPLITE.Size = new System.Drawing.Size(115, 46);
            this.btn_COMPLITE.TabIndex = 57;
            this.btn_COMPLITE.Text = "Завершить";
            this.btn_COMPLITE.UseVisualStyleBackColor = true;
            this.btn_COMPLITE.Click += new System.EventHandler(this.button2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(264, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 30);
            this.label3.TabIndex = 58;
            this.label3.Text = "Заказ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 15);
            this.label4.TabIndex = 59;
            this.label4.Text = "Официант";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(246, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 15);
            this.label5.TabIndex = 60;
            this.label5.Text = "Дата бронирования";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(368, 96);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 23);
            this.dateTimePicker1.TabIndex = 61;
            // 
            // OrderPP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 673);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_COMPLITE);
            this.Controls.Add(this.cb_waiters);
            this.Controls.Add(this.btn_CREATEorder);
            this.Controls.Add(this.tb_dateBron);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_placeDELETE);
            this.Controls.Add(this.btn_palacePUT);
            this.Controls.Add(this.btn_placePOST);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lb_menu);
            this.Controls.Add(this.btn_DELETE);
            this.Controls.Add(this.btn_REDACT);
            this.Controls.Add(this.btn_ADD);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tb_clientName);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OrderPP";
            this.Text = "Order";
            this.Load += new System.EventHandler(this.OrderPP_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_clientName;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lb_menu;
        private System.Windows.Forms.Button btn_DELETE;
        private System.Windows.Forms.Button btn_REDACT;
        private System.Windows.Forms.Button btn_ADD;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button btn_placeDELETE;
        private System.Windows.Forms.Button btn_palacePUT;
        private System.Windows.Forms.Button btn_placePOST;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker tb_dateBron;
        private System.Windows.Forms.Button btn_CREATEorder;
        private System.Windows.Forms.ComboBox cb_waiters;
        private System.Windows.Forms.Button btn_COMPLITE;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}