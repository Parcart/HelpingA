﻿namespace WinFormsApp.SecondFroms.PostPut.OrderAll
{
    partial class DishAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_dobav = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cb_Group = new System.Windows.Forms.ComboBox();
            this.tb_col = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cb_dish = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lb_price = new System.Windows.Forms.Label();
            this.lb_sum = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_dobav
            // 
            this.btn_dobav.Location = new System.Drawing.Point(133, 132);
            this.btn_dobav.Name = "btn_dobav";
            this.btn_dobav.Size = new System.Drawing.Size(110, 53);
            this.btn_dobav.TabIndex = 1;
            this.btn_dobav.Text = "Добавить";
            this.btn_dobav.UseVisualStyleBackColor = true;
            this.btn_dobav.Click += new System.EventHandler(this.btn_dobav_ClickAsync);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(54, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 21);
            this.label1.TabIndex = 2;
            this.label1.Text = "Создание записи";
            // 
            // cb_Group
            // 
            this.cb_Group.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_Group.FormattingEnabled = true;
            this.cb_Group.Location = new System.Drawing.Point(72, 45);
            this.cb_Group.Name = "cb_Group";
            this.cb_Group.Size = new System.Drawing.Size(165, 23);
            this.cb_Group.TabIndex = 3;
            this.cb_Group.SelectedIndexChanged += new System.EventHandler(this.cb_Group_SelectedIndexChanged);
            // 
            // tb_col
            // 
            this.tb_col.Location = new System.Drawing.Point(72, 103);
            this.tb_col.MaxLength = 2;
            this.tb_col.Name = "tb_col";
            this.tb_col.Size = new System.Drawing.Size(165, 23);
            this.tb_col.TabIndex = 2;
            this.tb_col.TextChanged += new System.EventHandler(this.tb_col_TextChanged);
            this.tb_col.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_price_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "Группа";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "Блюдо";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 15);
            this.label4.TabIndex = 8;
            this.label4.Text = "Количество";
            // 
            // cb_dish
            // 
            this.cb_dish.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_dish.FormattingEnabled = true;
            this.cb_dish.Location = new System.Drawing.Point(72, 74);
            this.cb_dish.Name = "cb_dish";
            this.cb_dish.Size = new System.Drawing.Size(165, 23);
            this.cb_dish.TabIndex = 9;
            this.cb_dish.SelectedIndexChanged += new System.EventHandler(this.cb_dish_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(0, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 15);
            this.label5.TabIndex = 10;
            this.label5.Text = "Цена:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(0, 167);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 15);
            this.label6.TabIndex = 11;
            this.label6.Text = "Сумма:";
            // 
            // lb_price
            // 
            this.lb_price.AutoSize = true;
            this.lb_price.Location = new System.Drawing.Point(44, 148);
            this.lb_price.Name = "lb_price";
            this.lb_price.Size = new System.Drawing.Size(0, 15);
            this.lb_price.TabIndex = 12;
            this.lb_price.TextAlignChanged += new System.EventHandler(this.lb_price_TextAlignChanged);
            // 
            // lb_sum
            // 
            this.lb_sum.AutoSize = true;
            this.lb_sum.Location = new System.Drawing.Point(54, 167);
            this.lb_sum.Name = "lb_sum";
            this.lb_sum.Size = new System.Drawing.Size(0, 15);
            this.lb_sum.TabIndex = 13;
            // 
            // DishAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(255, 190);
            this.Controls.Add(this.lb_sum);
            this.Controls.Add(this.lb_price);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cb_dish);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_col);
            this.Controls.Add(this.cb_Group);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_dobav);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DishAdd";
            this.Text = "Создание залла";
            this.Load += new System.EventHandler(this.Places_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_dobav;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cb_Group;
        private System.Windows.Forms.TextBox tb_col;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cb_dish;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lb_price;
        private System.Windows.Forms.Label lb_sum;
    }
}