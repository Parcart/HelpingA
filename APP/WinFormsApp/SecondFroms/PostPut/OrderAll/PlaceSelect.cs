﻿using Abstraction.Entity;
using REST;
using REST.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace WinFormsApp.SecondFroms.PostPut.OrderAll
{
    public partial class PlaceSelect : Form
    {
        public PlaceSelect()
        {
            InitializeComponent();
        }
        private Abstraction.Entity.PlacesRest placesRest;
        internal virtual Abstraction.Entity.PlacesRest PlacesRest { get { return placesRest; } set { placesRest = value; } }
        public enum SelectMethodEnum
        {
            Put,
            Post,
        }
        private SelectMethodEnum _method;
        public virtual SelectMethodEnum SelectMethod { get { return _method; } set { _method = value; } }
        private int? _selectid;
        public virtual int? Selectid { get { return _selectid; } set { _selectid = value; } }
        private int? _selectidOrder;
        public virtual int? SelectidOrder { get { return _selectid; } set { _selectid = value; } }
        private void AsyncMessageBox(Result result)
        {
            if (SelectMethod == SelectMethodEnum.Post)
            {

            }
            else if (SelectMethod == SelectMethodEnum.Put & result.Data.ToString() != "0")
            {

            }
            else
            {
                MessageBox.Show("Ошибка", "Ошибка");
            }
        }

        private void Places_Load(object sender, EventArgs e)
        {
            loadComboBox();
            if (SelectMethod == SelectMethodEnum.Post)
            {
                Text = "Создание записи";
                label1.Visible = false;
            }
            else if (SelectMethod == SelectMethodEnum.Put)
            {
                //textBox1.Text = Selectid.ToString();
                cb_hall.Text = getName(_hall, PlacesRest.Hall_idid);
                cb_place.Text = PlacesRest.Table;
                Text = "Обновление записи";
                btn_dobav.Text = "Вставить";
            }
            else
            {
                Text = "Ошибка в создании формы";
            }
        }
        private Dictionary<long, string> _hall = new Dictionary<long, string>();
        private Dictionary<long, string> _place = new Dictionary<long, string>();
        private void loadComboBox()
        {
            _hall.Clear();
            cb_hall.Items.Clear();
            Result Groupget = RestAPI.Instance.Hall.Get();
            List<Abstraction.Entity.Hall> Hall = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Hall>>(Groupget.Data.ToString());
            Hall.ForEach(cell =>
            {
                _hall.Add(cell.Hall_id, cell.Name);
                cb_hall.Items.Add(cell.Name);
            });


        }
        private long getID(Dictionary<long, string> dict, ComboBox comboBox)
        {
            long id = 0;
            foreach (var name in dict)
            {
                if (name.Value == comboBox.Text.Split(", ")[0])
                {
                    id = name.Key;
                    return id;
                }
            }
            return id;
        }
        private string getName(Dictionary<long, string> dict, long id)
        {
            string name = "";
            foreach (var row in dict)
            {
                if (row.Key == id)
                {
                    name = row.Value;
                    return name;
                }
            }
            return name;
        }
        private async void btn_dobav_ClickAsync(object sender, EventArgs e)
        {
            if (cb_place.Text != "" && cb_hall.Text != "")
            {
                Abstraction.Entity.PlacesRest _PlacesRest = new Abstraction.Entity.PlacesRest();
                _PlacesRest.Hall_idid = getID(_hall, cb_hall);
                _PlacesRest.Hall_id = cb_hall.Text;
                _PlacesRest.Table = cb_place.Text;
                _PlacesRest.Places_id = getID(_place, cb_place);
                PlacesRest = _PlacesRest;
                Close();
            }
            else
            {
                MessageBox.Show("Не все данные заполнены");
            }
        }

        private void bt_dobavhall_Click(object sender, EventArgs e)
        {
            Hall hall = new Hall();
            hall.SelectMethod = Hall.SelectMethodEnum.Post;
            hall.ShowDialog();
            loadComboBox();
        }

        private void cb_hall_SelectedIndexChanged(object sender, EventArgs e)
        {
            _place.Clear();
            cb_place.Items.Clear();
            Result result = RestAPI.Instance.Places.Get();
            List<Abstraction.Entity.Places> dishn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Places>>(result.Data.ToString());

            Result orderget = RestAPI.Instance.Order.Get();
            List<Abstraction.Entity.Order> order = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Order>>(orderget.Data.ToString());
            List<Abstraction.Entity.Order> orders = new List<Order>();
            orders = order.Where(cell => cell.complite == "0" || (cell.IsReservation == "1" && cell.complite == "0")).ToList();

            Result orderplacehget = RestAPI.Instance.Order_places.Get();
            List<Abstraction.Entity.Order_places> orderplace = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Order_places>>(orderplacehget.Data.ToString());

            List<Abstraction.Entity.Order_places> placeO = new List<Order_places>();
            orderplace.ForEach(cl =>
            {
                orders.ForEach(ord =>
                {
                    if (cl.Order_id == ord.Order_id)
                    {
                        var palceClass = new Order_places();
                        palceClass.id = Convert.ToInt64(ord.IsReservation);
                        palceClass.Order_id = ord.Order_id;
                        palceClass.Places_id = cl.Places_id;
                        placeO.Add(palceClass);
                    }
                });
            });
            var forec1 = dishn.Where(cell => cell.Hall_id == getID(_hall, cb_hall)).ToList();
            foreach (var cell in forec1)
            {
                bool itemcompl = false;
                bool skeep = false;
                placeO.ForEach(pl =>
                {
                    if (pl.Order_id == Selectid)
                    {
                        if (cell.Places_id == pl.Places_id)
                        {
                            skeep = true;

                        }
                    }
                });
                //if (skeep)
                //{
                //    continue;
                //}
                _place.Add(cell.Places_id, cell.stolik);
                List<string> adder = new List<string>();

                placeO.ForEach(pl =>
                {
                    if (cell.Places_id == pl.Places_id)
                    {
                        itemcompl = true;
                        if (pl.id == 1)
                        {
                            adder.Add($"бронирование на {orders.Where(ff => ff.Order_id == pl.Order_id).FirstOrDefault().DateReservation.ToString()}");
                        }
                        if (pl.id == 0)
                        {
                            adder.Add($"занят нз:{pl.Order_id}");
                        }
                    }
                });
                if (itemcompl)
                {
                    string strcomp = cell.stolik;
                    adder.ForEach(mda =>
                    {
                        strcomp = strcomp + ", " + mda;
                    });
                    cb_place.Items.Add(strcomp);
                }
                if (!itemcompl)
                {
                    cb_place.Items.Add(cell.stolik);
                }
            };
        }
    }
}
