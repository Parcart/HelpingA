﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using REST;
using REST.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp.SecondFroms.PostPut.OrderAll
{
    public partial class OrderPP : Form
    {
        public OrderPP()
        {
            InitializeComponent();
        }
        public enum SelectMethodEnum
        {
            Put,
            Post,
        }
        public enum SelectOrderWEnum
        {
            Reserv,
            NO,
        }
        private SelectOrderWEnum _methodorder;
        public virtual SelectOrderWEnum SelectOrder { get { return _methodorder; } set { _methodorder = value; } }
        private List<Abstraction.Entity.Order_dishRest> _orderDishRestList;
        private List<Abstraction.Entity.PlacesRest> _PlaceRestList;
        private SelectMethodEnum _method;
        public virtual SelectMethodEnum SelectMethod { get { return _method; } set { _method = value; } }
        private int? _selectid;
        public virtual int? Selectid { get { return _selectid; } set { _selectid = value; } }


        private void btn_DELETE_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow != null)
            {
                changeTable1 = true;
                _orderDishRestList.RemoveAt(Convert.ToInt32(dataGridView1.CurrentRow.Index.ToString()));
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = _orderDishRestList.GetRange(0, _orderDishRestList.Count);
                dataGridView1.Columns[0].Visible = false;
                dataGridView1.Columns[1].Visible = false;
                dataGridView1.Columns[2].Visible = false;
                dataGridView1.Columns[3].HeaderText = "Товар";
                dataGridView1.Columns[4].HeaderText = "Количество";
                dataGridView1.Columns[5].HeaderText = "Цена";
                dataGridView1.Columns[6].HeaderText = "Сумма";
                Itog();
            }
            else
            {
                MessageBox.Show("Строка не выбрана");
            }
        }



        private void btn_REDACT_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow != null)
            {
                _OLDorderDishRestList = _orderDishRestList.ToList();
                changeTable1 = true;
                DishAdd dishAdd = new DishAdd();
                dishAdd.SelectMethod = DishAdd.SelectMethodEnum.Put;
                Abstraction.Entity.Order_dishRest order_DishRest = new Abstraction.Entity.Order_dishRest();
                order_DishRest.price = Convert.ToDouble(dataGridView1.CurrentRow.Cells[5].Value.ToString());
                order_DishRest.Amount = Convert.ToInt64(dataGridView1.CurrentRow.Cells[4].Value.ToString());
                order_DishRest.Dish_id = Convert.ToInt64(dataGridView1.CurrentRow.Cells[2].Value.ToString());
                order_DishRest.Dish_name = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                dishAdd.Order_dishRest = order_DishRest;
                dishAdd.Selectid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[2].Value.ToString());
                dishAdd.ShowDialog();
                _orderDishRestList[Convert.ToInt32(dataGridView1.CurrentRow.Index.ToString())] = dishAdd.Order_dishRest;
                GroupByTables();
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = _orderDishRestList.GetRange(0, _orderDishRestList.Count);
                dataGridView1.Columns[0].Visible = false;
                dataGridView1.Columns[1].Visible = false;
                dataGridView1.Columns[2].Visible = false;
                dataGridView1.Columns[3].HeaderText = "Товар";
                dataGridView1.Columns[4].HeaderText = "Количество";
                dataGridView1.Columns[5].HeaderText = "Цена";
                dataGridView1.Columns[6].HeaderText = "Сумма";
                Itog();
            }
            else
            {
                MessageBox.Show("Строка не выбрана");
            }
        }
        
        private void btn_ADD_Click(object sender, EventArgs e)
        {
            _OLDorderDishRestList = _orderDishRestList.ToList();
            DishAdd dishAdd = new DishAdd();
            dishAdd.SelectMethod = DishAdd.SelectMethodEnum.Post;
            dishAdd.ShowDialog();
            if (dishAdd.Order_dishRest != null)
            {
                changeTable1 = true;
                _orderDishRestList.Add(dishAdd.Order_dishRest);
                GroupByTables();
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = _orderDishRestList.GetRange(0, _orderDishRestList.Count);
                dataGridView1.Columns[0].Visible = false;
                dataGridView1.Columns[1].Visible = false;
                dataGridView1.Columns[2].Visible = false;
                dataGridView1.Columns[3].HeaderText = "Товар";
                dataGridView1.Columns[4].HeaderText = "Количество";
                dataGridView1.Columns[5].HeaderText = "Цена";
                dataGridView1.Columns[6].HeaderText = "Сумма";
                Itog();
            }
        }
        private List<Abstraction.Entity.Order_dishRest> _OLDorderDishRestList;
        private void Itog()
        {
            int count = 0;
            float Amount = 0;
            double Price = 0;
            foreach (var row in _orderDishRestList)
            {
                count++;
                Price += row.sumpa;
                Amount += row.Amount;
            }
            label9.Text = count.ToString();
            label12.Text = Amount.ToString();
            label8.Text = Price.ToString();
            label9.Visible = true;
            label12.Visible = true;
            label8.Visible = true;
        }
        private void GroupByTables()
        {
            _orderDishRestList = _orderDishRestList.GroupBy(c => c.Dish_id)
               .Select(cl => new Abstraction.Entity.Order_dishRest
               {
                   Dish_id = cl.First().Dish_id,
                   Dish_name = cl.First().Dish_name,
                   Amount = cl.Sum(c => c.Amount),
                   price = cl.First().price,
                   sumpa = cl.Sum(c => c.price * c.Amount),
               }).ToList();
            if (0 < _orderDishRestList.Where(cell => cell.Amount > 99).Count())
            {
                _orderDishRestList = _OLDorderDishRestList;
                MessageBox.Show("Максимальное количество 99");
            }
        }
        private void GroupByTables2()
        {
            _PlaceRestList = _PlaceRestList.GroupBy(c => c.Places_id)
               .Select(cl => new Abstraction.Entity.PlacesRest
               {
                   Places_id = cl.First().Places_id,
                   Hall_id = cl.First().Hall_id,
                   Hall_idid = cl.First().Hall_idid,
                   Table = cl.First().Table,
               }).ToList();
        }

        private List<int> dishids = new List<int>();
        private List<int> placeids = new List<int>();

        private void OrderPP_Load(object sender, EventArgs e)
        {
            loadComboBox();
            List<Abstraction.Entity.Order_dishRest> _orderDishRestListf = new List<Abstraction.Entity.Order_dishRest>();
            List<Abstraction.Entity.PlacesRest> PlacesRestList = new List<Abstraction.Entity.PlacesRest>();
            _orderDishRestList = _orderDishRestListf;
            _PlaceRestList = PlacesRestList;
            if (SelectMethod == SelectMethodEnum.Post)
            {
                if (SelectOrder == SelectOrderWEnum.Reserv)
                {
                    btn_CREATEorder.Text = "Забронировать";
                    dateTimePicker1.Value = DateTime.Now;
                    Text = "Бронирование";
                    isReservation = true;
                }
                else
                {
                    Text = "Создание заказа";
                    label5.Visible = false;
                    dateTimePicker1.Visible = false;
                }
                btn_COMPLITE.Visible = false;
                dataGridView1.DataSource = _orderDishRestList;
                dataGridView1.Columns[0].Visible = false;
                dataGridView1.Columns[1].Visible = false;
                dataGridView1.Columns[2].Visible = false;
                dataGridView1.Columns[3].HeaderText = "Товар";
                dataGridView1.Columns[4].HeaderText = "Количество";
                dataGridView1.Columns[5].HeaderText = "Сумма";
                dataGridView1.Columns[6].Visible = false;
                dataGridView1.Refresh();
            }
            else if (SelectMethod == SelectMethodEnum.Put)
            {
                Result orderget = RestAPI.Instance.Order.Get((long)Selectid);
                Abstraction.Entity.Order order = Newtonsoft.Json.JsonConvert.DeserializeObject<Abstraction.Entity.Order>(orderget.Data.ToString());
                if (order.complite == "1") 
                {
                    btn_COMPLITE.Text = "Завершон";
                    btn_COMPLITE.Enabled = false;
                    btn_CREATEorder.Enabled = false;
                    tb_clientName.Enabled = false;
                    button1.Enabled = false;
                    cb_waiters.Enabled = false;
                    dateTimePicker1.Enabled = false;
                    btn_placePOST.Enabled = false;
                    btn_palacePUT.Enabled = false;
                    btn_placeDELETE.Enabled = false;
                    btn_ADD.Enabled = false;
                    btn_REDACT.Enabled = false;
                    btn_DELETE.Enabled = false;

                }
                btn_CREATEorder.Text = "Изменить";
                Text = "Обновление записи";
                Result orderdishget = RestAPI.Instance.Order_dish.Get();
                List<Abstraction.Entity.Order_dish> orderdish = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Order_dish>>(orderdishget.Data.ToString());

                Result orderplacehget = RestAPI.Instance.Order_places.Get();
                List<Abstraction.Entity.Order_places> orderplace = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Order_places>>(orderplacehget.Data.ToString());

                Result Groupget = RestAPI.Instance.Waiters.Get();
                List<Abstraction.Entity.Waiters> Waiters = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Waiters>>(Groupget.Data.ToString());

                Result dishrs = RestAPI.Instance.Dish.Get();
                List<Abstraction.Entity.Dish> dish = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Dish>>(dishrs.Data.ToString());

                Result placeget = RestAPI.Instance.Places.Get();
                List<Abstraction.Entity.Places> places = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Places>>(placeget.Data.ToString());

                Result hallget = RestAPI.Instance.Hall.Get();
                List<Abstraction.Entity.Hall> hall = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Hall>>(hallget.Data.ToString());

                if (order.IsReservation == "1") { isReservation = true; }
                if (order.complite == "1") { complite = true; }
                _orderDishRestList = orderdish.Where(cell => cell.Order_id == Selectid)
                    .Select(cl => new Abstraction.Entity.Order_dishRest
                    {
                        id = cl.id,
                        Order_id = cl.Order_id,
                        Dish_id = cl.Dish_id,
                        Dish_name = dish.Where(cldish => cldish.Dish_Id == cl.Dish_id).FirstOrDefault().Name,
                        Amount = cl.Amount,
                        price = cl.price,
                        sumpa = cl.Amount * cl.price
                    }).ToList();

                _orderDishRestList.ForEach(cl =>
                {
                    dishids.Add((int)cl.id);
                });
                _PlaceRestList = orderplace.Where(cell => cell.Order_id == Selectid)
                    .Select(cl => new Abstraction.Entity.PlacesRest
                    {
                        id = cl.id,
                        Places_id = cl.Places_id,
                        Hall_idid = places.Where(pl => pl.Places_id == cl.Places_id).FirstOrDefault().Hall_id,
                        Hall_id = hall.Where(pl => pl.Hall_id == places.Where(pl => pl.Places_id == cl.Places_id).FirstOrDefault().Hall_id).FirstOrDefault().Name,
                        Table = places.Where(pl => pl.Places_id == cl.Places_id).FirstOrDefault().stolik
                    }).ToList();
                _PlaceRestList.ForEach(cl =>
                {
                    placeids.Add((int)cl.id);
                });
                dataGridView2.DataSource = null;
                dataGridView2.DataSource = _PlaceRestList.GetRange(0, _PlaceRestList.Count);
                dataGridView2.Columns[0].Visible = false;
                dataGridView2.Columns[1].Visible = false;
                dataGridView2.Columns[2].HeaderText = "Зал";
                dataGridView2.Columns[3].HeaderText = "Место";
                dataGridView2.Columns[4].Visible = false;
                Itog();
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = _orderDishRestList.GetRange(0, _orderDishRestList.Count);
                dataGridView1.Columns[0].Visible = false;
                dataGridView1.Columns[1].Visible = false;
                dataGridView1.Columns[2].Visible = false;
                dataGridView1.Columns[3].HeaderText = "Товар";
                dataGridView1.Columns[4].HeaderText = "Количество";
                dataGridView1.Columns[5].HeaderText = "Цена";
                dataGridView1.Columns[6].HeaderText = "Сумма";

                tb_clientName.Text = order.clientName;
                OrderReservStart = order.OrderReservStart;
                tb_dateBron.Value = order.Data;
                selectedClientID = order.client_id;
                var waiter = Waiters.Where(wt => wt.Waiter_id == order.Waiter_ID).FirstOrDefault();
                cb_waiters.Text = $"{waiter.Waiter_id}, {waiter.FIO}";
                if (SelectOrder == SelectOrderWEnum.Reserv)
                {
                    Text = "Бронирование";
                    isReservation = true;
                    dateTimePicker1.Value = (DateTime)order.DateReservation;
                }
                else
                {
                    Text = "Изменение заказа";
                    label5.Visible = false;
                    dateTimePicker1.Visible = false;
                }
            }
        }
        private string OrderReservStart;
        private void loadComboBox()
        {
            cb_waiters.Items.Clear();
            Result Groupget = RestAPI.Instance.Waiters.Get();
            List<Abstraction.Entity.Waiters> Group = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Waiters>>(Groupget.Data.ToString());
            Group.ForEach(cell =>
            {
                cb_waiters.Items.Add($"{cell.Waiter_id}, {cell.FIO}");
            });
        }
        private long getID(Dictionary<long, string> dict, ComboBox comboBox)
        {
            long id = 0;
            foreach (var name in dict)
            {
                if (name.Value == comboBox.Text)
                {
                    id = name.Key;
                    return id;
                }
            }
            return id;
        }
        private string getName(Dictionary<long, string> dict, long id)
        {
            string name = "";
            foreach (var row in dict)
            {
                if (row.Key == id)
                {
                    name = row.Value;
                    return name;
                }
            }
            return name;
        }
        private bool changeTable1;
        private bool changeTable2;

        private void btn_placePOST_Click(object sender, EventArgs e)
        {
            PlaceSelect place = new PlaceSelect();
            place.SelectMethod = PlaceSelect.SelectMethodEnum.Post;
            place.ShowDialog();
            if (place.PlacesRest != null)
            {
                place.PlacesRest.Table = place.PlacesRest.Table.Split(", ")[0];
                changeTable2 = true;
                _PlaceRestList.Add(place.PlacesRest);
                GroupByTables2();
                dataGridView2.DataSource = null;
                dataGridView2.DataSource = _PlaceRestList.GetRange(0, _PlaceRestList.Count);
                dataGridView2.Columns[0].Visible = false;
                dataGridView2.Columns[1].Visible = false;
                dataGridView2.Columns[2].HeaderText = "Зал";
                dataGridView2.Columns[3].HeaderText = "Место";
                dataGridView2.Columns[4].Visible = false;
            }
        }
        private void btn_palacePUT_Click(object sender, EventArgs e)
        {
            if (dataGridView2.CurrentRow != null)
            {
                changeTable2 = true;
                PlaceSelect place = new PlaceSelect();
                place.SelectMethod = PlaceSelect.SelectMethodEnum.Put;
                Abstraction.Entity.PlacesRest PlacesRest = new Abstraction.Entity.PlacesRest();
                PlacesRest.Places_id = Convert.ToInt64(dataGridView2.CurrentRow.Cells[0].Value.ToString());
                PlacesRest.Hall_idid = Convert.ToInt64(dataGridView2.CurrentRow.Cells[1].Value.ToString());
                PlacesRest.Hall_id = dataGridView2.CurrentRow.Cells[2].Value.ToString();
                PlacesRest.Table = dataGridView2.CurrentRow.Cells[3].Value.ToString();
                place.PlacesRest = PlacesRest;
                place.Selectid = Convert.ToInt32(dataGridView2.CurrentRow.Cells[0].Value.ToString());
                place.ShowDialog();
                place.PlacesRest.Table = place.PlacesRest.Table.Split(", ")[0];
                _PlaceRestList[Convert.ToInt32(dataGridView2.CurrentRow.Index.ToString())] = place.PlacesRest;
                GroupByTables();
                dataGridView2.DataSource = null;
                dataGridView2.DataSource = _PlaceRestList.GetRange(0, _PlaceRestList.Count);
                dataGridView2.Columns[0].Visible = false;
                dataGridView2.Columns[1].Visible = false;
                dataGridView2.Columns[2].HeaderText = "Зал";
                dataGridView2.Columns[3].HeaderText = "Место";
                dataGridView2.Columns[4].Visible = false;
                Itog();
            }
            else
            {
                MessageBox.Show("Строка не выбрана");
            }
        }

        private void btn_placeDELETE_Click(object sender, EventArgs e)
        {
            if (dataGridView2.CurrentRow != null)
            {
                changeTable2 = true;
                _PlaceRestList.RemoveAt(Convert.ToInt32(dataGridView2.CurrentRow.Index.ToString()));
                dataGridView2.DataSource = null;
                dataGridView2.DataSource = _PlaceRestList.GetRange(0, _PlaceRestList.Count);
                dataGridView2.Columns[0].Visible = false;
                dataGridView2.Columns[1].Visible = false;
                dataGridView2.Columns[2].HeaderText = "Зал";
                dataGridView2.Columns[3].HeaderText = "Место";
                dataGridView2.Columns[4].Visible = false;
                Itog();
            }
            else
            {
                MessageBox.Show("Строка не выбрана");
            }
        }

        private bool selectedClient;
        private long? selectedClientID;
        private bool selectedWaiter;
        private long selectedWaiterID;
        private void button1_Click(object sender, EventArgs e)
        {
            ClientSelect clientSelect = new ClientSelect();
            clientSelect.ShowDialog();
            if (clientSelect.DialogResult == DialogResult.OK)
            {
                selectedClient = true;
                selectedClientID = Convert.ToInt64(clientSelect.SelectClient.Split(", ")[0]);
                tb_clientName.Text = clientSelect.SelectClient.Split(", ")[1];
            }
        }

        private void cb_waiters_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedWaiter = true;
            selectedWaiterID = Convert.ToInt64(cb_waiters.Text.Split(", ")[0]);
        }
        private bool complite;
        private bool isReservation;
        public virtual bool isReservationCl { get { return isReservation; } set { isReservation = value; } }

        private async void btn_CREATEorder_Click(object sender, EventArgs e)
        {
            postput();
        }

        private async void postput()
        {
            DateTime? dateTime;
            if (SelectOrder == SelectOrderWEnum.Reserv)
            {
                dateTime = DateTime.ParseExact(dateTimePicker1.Value.ToString(), "dd.MM.yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                dateTime = null;
            }
            if (dataGridView2.Rows.Count != 0)
            {
                if (tb_clientName.Text != "" && selectedWaiter)
                {
                    if (SelectMethod == SelectMethodEnum.Put)
                    {
                        JObject jsonObject = new JObject();
                        jsonObject.Add("order_id", 0);
                        jsonObject.Add("client_id", selectedClientID == null ? null : selectedClientID);
                        jsonObject.Add("clientName", tb_clientName.Text);
                        jsonObject.Add("data", tb_dateBron.Value.ToString("yyyy-MM-ddTHH:mm:ss"));
                        jsonObject.Add("Waiter_ID", Convert.ToInt64(cb_waiters.Text.Split(", ")[0]));
                        jsonObject.Add("totalPrice", 0);
                        jsonObject.Add("complite", complite ? "1" : "0");
                        jsonObject.Add("isReservation", isReservation ? "1" : "0");
                        jsonObject.Add("dateReservation", dateTime);
                        jsonObject.Add("OrderReservStart", OrderReservStart);
                        string json = JsonConvert.SerializeObject(jsonObject);
                        Result result = await Task.Run(() => RestAPI.Instance.Order.Put((long)Selectid, json));
                        if (changeTable1)
                        {
                            foreach (var rowstart in dishids)
                            {
                                Result resultPoz = RestAPI.Instance.Order_dish.Delete(rowstart);
                            }
                            foreach (var row in _orderDishRestList)
                            {
                                JObject jsonObjectDish = new JObject();
                                jsonObjectDish.Add("id", 0);
                                jsonObjectDish.Add("order_id", (long)Selectid);
                                jsonObjectDish.Add("dish_id", row.Dish_id);
                                jsonObjectDish.Add("amount", row.Amount);
                                jsonObjectDish.Add("price", row.price);
                                string jsonDish = JsonConvert.SerializeObject(jsonObjectDish);
                                Result resultPoz = await Task.Run(() => RestAPI.Instance.Order_dish.Post(jsonDish));
                            }
                        }
                        if (changeTable2)
                        {
                            foreach (var rowstart in placeids)
                            {
                                Result resultPoz = RestAPI.Instance.Order_places.Delete(rowstart);
                            }
                            foreach (var row in _PlaceRestList)
                            {
                                JObject jsonObjectPoz = new JObject();
                                jsonObjectPoz.Add("id", 0);
                                jsonObjectPoz.Add("order_id", (long)Selectid);
                                jsonObjectPoz.Add("places_id", row.Places_id);
                                string jsonPlace = JsonConvert.SerializeObject(jsonObjectPoz);
                                Result resultPoz = await Task.Run(() => RestAPI.Instance.Order_places.Post(jsonPlace));
                            }
                        }
                        MessageBox.Show("Успешно");
                        Close();
                    }
                    else if (SelectMethod == SelectMethodEnum.Post)
                    {
                        JObject jsonObject = new JObject();
                        jsonObject.Add("order_id", 0);
                        jsonObject.Add("client_id", selectedClientID == null ? null : selectedClientID);
                        jsonObject.Add("clientName", tb_clientName.Text);
                        jsonObject.Add("data", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
                        jsonObject.Add("Waiter_ID", Convert.ToInt64(cb_waiters.Text.Split(", ")[0]));
                        jsonObject.Add("totalPrice", 0);
                        jsonObject.Add("complite", "0");
                        jsonObject.Add("isReservation", isReservation ? "1" : "0");
                        jsonObject.Add("dateReservation", dateTime);
                        jsonObject.Add("OrderReservStart", "0");
                        string json = JsonConvert.SerializeObject(jsonObject);
                        Result result = await Task.Run(() => RestAPI.Instance.Order.Post(json));
                        if (result.Successful == true)
                        {
                            foreach (var row in _PlaceRestList)
                            {
                                JObject jsonObjectPoz = new JObject();
                                jsonObjectPoz.Add("id", 0);
                                jsonObjectPoz.Add("order_id", Convert.ToUInt32(result.Data));
                                jsonObjectPoz.Add("places_id", row.Places_id);
                                string jsonPlace = JsonConvert.SerializeObject(jsonObjectPoz);
                                Result resultPoz = await Task.Run(() => RestAPI.Instance.Order_places.Post(jsonPlace));
                            }
                            foreach (var row in _orderDishRestList)
                            {
                                JObject jsonObjectDish = new JObject();
                                jsonObjectDish.Add("id", 0);
                                jsonObjectDish.Add("order_id", Convert.ToUInt32(result.Data));
                                jsonObjectDish.Add("dish_id", row.Dish_id);
                                jsonObjectDish.Add("amount", row.Amount);
                                jsonObjectDish.Add("price", row.price);
                                string jsonDish = JsonConvert.SerializeObject(jsonObjectDish);
                                Result resultPoz = await Task.Run(() => RestAPI.Instance.Order_dish.Post(jsonDish));
                            }
                            MessageBox.Show("Успешно");
                            Close();
                        }
                        else
                        {
                            MessageBox.Show("Ошибка при записи");
                        }


                    }
                    else
                    {
                        MessageBox.Show("Ошибка");
                    }
                }
                else
                {
                    MessageBox.Show("Не все данные заполнены");
                }
            }
            else
            {
                MessageBox.Show("Места не выбраны");
            }
        }
        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            complite = true;
            postput();
        }

        private void tb_clientName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsControl(e.KeyChar) || char.IsLetter(e.KeyChar))
            {
                return;
            }
            e.Handled = true;
        }
    }
}
