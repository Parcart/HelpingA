﻿using REST.Entity;
using REST;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WinFormsApp.SecondFroms.PostPut.OrderAll
{
    public partial class ClientSelect : Form
    {
        public ClientSelect()
        {
            InitializeComponent();
        }
        public enum SelectMethodEnum
        {
            Put,
            Post,
        }
        private string _selectClient;
        public virtual string SelectClient { get { return _selectClient; } set { _selectClient = value; } }
        private void btn_select_Click(object sender, EventArgs e)
        {
            SelectClient = cb_client.Text;
            DialogResult = DialogResult.OK;
            Close();
        }
        private void ClientSelect_Load(object sender, EventArgs e)
        {
            cb_client.Items.Clear();
            Result Groupget = RestAPI.Instance.Client.Get();
            List<Abstraction.Entity.Client> Group = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Client>>(Groupget.Data.ToString());
            Group.ForEach(cell =>
            {
                cb_client.Items.Add($"{cell.Client_id}, "+ cell.FIO + $", {cell.TelephoneNumber}");
            });
        }
    }
}
