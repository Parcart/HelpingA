﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using REST.Entity;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using REST;
using ComboBox = System.Windows.Forms.ComboBox;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Abstraction.Entity;

namespace WinFormsApp.SecondFroms.PostPut
{
    public partial class Places : Form
    {
        public Places()
        {
            InitializeComponent();
        }
        public enum SelectMethodEnum
        {
            Put,
            Post,
        }
        private SelectMethodEnum _method;
        public virtual SelectMethodEnum SelectMethod { get { return _method; } set { _method = value; } }
        private int? _selectid;
        public virtual int? Selectid { get { return _selectid; } set { _selectid = value; } }
        private void AsyncMessageBox(Result result)
        {
            if (SelectMethod == SelectMethodEnum.Post)
            {

            }
            else if (SelectMethod == SelectMethodEnum.Put & result.Data.ToString() != "0")
            {

            }
            else
            {
                MessageBox.Show("Ошибка", "Ошибка");
            }
        }
        private string startname;
        private void Places_Load(object sender, EventArgs e)
        {
            loadComboBox();
            if (SelectMethod == SelectMethodEnum.Post)
            {
                startname = "";
                Text = "Создание записи";
            }
            else if (SelectMethod == SelectMethodEnum.Put)
            {
                //textBox1.Text = Selectid.ToString();
                Result result = RestAPI.Instance.Places.Get((long)Selectid);
                Abstraction.Entity.Places hall = Newtonsoft.Json.JsonConvert.DeserializeObject<Abstraction.Entity.Places>(result.Data.ToString());
                tb_name.Text = hall.stolik;
                startname = hall.stolik;
                cb_hall.Text = getName(unit, hall.Hall_id);
                Text = "Обновление записи";
                btn_dobav.Text = "Вставить";
            }
            else
            {
                Text = "Ошибка в создании формы";
            }
        }
        private Dictionary<long, string> unit = new Dictionary<long, string>();
        private void loadComboBox() 
        {
            unit.Clear();
            cb_hall.Items.Clear();
            Result hallget = RestAPI.Instance.Hall.Get();
            List<Abstraction.Entity.Hall> hall = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Hall>>(hallget.Data.ToString());
            hall.ForEach(cell =>
            {
                unit.Add(cell.Hall_id, cell.Name);
                cb_hall.Items.Add(cell.Name);
            });

        }
        private long getID(Dictionary<long, string> dict, ComboBox comboBox)
        {
            long id = 0;
            foreach (var name in dict)
            {
                if (name.Value == comboBox.Text)
                {
                    id = name.Key;
                    return id;
                }
            }
            return id;
        }
        private string getName(Dictionary<long, string> dict, long id)
        {
            string name = "";
            foreach (var row in dict)
            {
                if (row.Key == id)
                {
                    name = row.Value;
                    return name;
                }
            }
            return name;
        }
        private async void btn_dobav_ClickAsync(object sender, EventArgs e)
        {
            if (tb_name.Text != "" && cb_hall.Text != "")
            {
                Result Places = RestAPI.Instance.Places.Get();
                List<Abstraction.Entity.Places> places = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Places>>(Places.Data.ToString());

                int wow = places.Where(cell => cell.Hall_id == getID(unit, cb_hall) && cell.stolik == tb_name.Text).Count();
                if (wow == 0 || (SelectMethod == SelectMethodEnum.Put && startname == tb_name.Text))
                {
                    long needID = getID(unit, cb_hall);
                    if (needID != 0)
                    {
                        JObject jsonObject = new JObject();
                        jsonObject.Add("places_id", 0);
                        jsonObject.Add("Hall_id", getID(unit, cb_hall));
                        jsonObject.Add("stolik", tb_name.Text);
                        string json = JsonConvert.SerializeObject(jsonObject);
                        if (SelectMethod == SelectMethodEnum.Put)
                        {
                            Result result = await Task.Run(() => RestAPI.Instance.Places.Put((long)Selectid, json));
                            AsyncMessageBox(result);
                            Close();
                        }
                        else if (SelectMethod == SelectMethodEnum.Post)
                        {
                            Result result = await Task.Run(() => RestAPI.Instance.Places.Post(json));
                            AsyncMessageBox(result);
                            Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Такого зала не существует!");
                    }
                }
                else
                {
                    MessageBox.Show("Данный столик уже существует");
                }
            }
            else
            {
                MessageBox.Show("Не все данные заполнены");
            }
        }

        private void bt_dobavhall_Click(object sender, EventArgs e)
        {
            Hall hall = new Hall();
            hall.SelectMethod = Hall.SelectMethodEnum.Post;
            hall.ShowDialog();
            loadComboBox();
            cb_hall.Text = hall.Newid;
        }
    }
}
