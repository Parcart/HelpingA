﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using REST.Entity;
using REST;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;

namespace WinFormsApp.SecondFroms.PostPut
{
    public partial class Dish : Form
    {
        public Dish()
        {
            InitializeComponent();
        }
        public enum SelectMethodEnum
        {
            Put,
            Post,
        }
        private SelectMethodEnum _method;
        public virtual SelectMethodEnum SelectMethod { get { return _method; } set { _method = value; } }
        private int? _selectid;
        public virtual int? Selectid { get { return _selectid; } set { _selectid = value; } }
        private void AsyncMessageBox(Result result)
        {
            if (SelectMethod == SelectMethodEnum.Post)
            {

            }
            else if (SelectMethod == SelectMethodEnum.Put & result.Data.ToString() != "0")
            {

            }
            else
            {
                MessageBox.Show("Ошибка", "Ошибка");
            }
        }
        private string startname;
        private void Places_Load(object sender, EventArgs e)
        {
            loadComboBox();
            if (SelectMethod == SelectMethodEnum.Post)
            {
                startname = "";
                Text = "Создание записи";
            }
            else if (SelectMethod == SelectMethodEnum.Put)
            {
                //textBox1.Text = Selectid.ToString();
                Result result = RestAPI.Instance.Dish.Get((long)Selectid);
                Abstraction.Entity.Dish dish = Newtonsoft.Json.JsonConvert.DeserializeObject<Abstraction.Entity.Dish>(result.Data.ToString());
                tb_name.Text = dish.Name;
                startname = dish.Name;
                tb_price.Text = dish.Price.ToString();
                cb_Group.Text = getName(unit, dish.Group_id);
                Text = "Обновление записи";
                btn_dobav.Text = "Вставить";
            }
            else
            {
                Text = "Ошибка в создании формы";
            }
        }
        private Dictionary<long, string> unit = new Dictionary<long, string>();
        private void loadComboBox()
        {
            unit.Clear();
            cb_Group.Items.Clear();
            Result Groupget = RestAPI.Instance.Group.Get();
            List<Abstraction.Entity.Group> dish = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Group>>(Groupget.Data.ToString());
            dish.ForEach(cell =>
            {
                unit.Add(cell.Group_id, cell.Name);
                cb_Group.Items.Add(cell.Name);
            });

        }
        private long getID(Dictionary<long, string> dict, ComboBox comboBox)
        {
            long id = 0;
            foreach (var name in dict)
            {
                if (name.Value == comboBox.Text)
                {
                    id = name.Key;
                    return id;
                }
            }
            return id;
        }
        private string getName(Dictionary<long, string> dict, long id)
        {
            string name = "";
            foreach (var row in dict)
            {
                if (row.Key == id)
                {
                    name = row.Value;
                    return name;
                }
            }
            return name;
        }
        private async void btn_dobav_ClickAsync(object sender, EventArgs e)
        {
            if (tb_name.Text != "" && cb_Group.Text != "" && (tb_price.Text != "" & tb_price.Text != "0"))
            {
                Result Places = RestAPI.Instance.Dish.Get();
                List<Abstraction.Entity.Dish> dish = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Dish>>(Places.Data.ToString());

                int wow = dish.Where(cell => cell.Group_id == getID(unit, cb_Group) && cell.Name == tb_name.Text).Count();
                if (wow == 0 || (SelectMethod == SelectMethodEnum.Put && startname == tb_name.Text))
                {
                    long needID = getID(unit, cb_Group);
                    if (needID != 0)
                    {
                        JObject jsonObject = new JObject();
                        jsonObject.Add("dish_Id", 0);
                        jsonObject.Add("group_id", getID(unit, cb_Group));
                        jsonObject.Add("name", tb_name.Text);
                        jsonObject.Add("price", Convert.ToInt64(tb_price.Text));
                        string json = JsonConvert.SerializeObject(jsonObject);
                        if (SelectMethod == SelectMethodEnum.Put)
                        {
                            Result result = await Task.Run(() => RestAPI.Instance.Dish.Put((long)Selectid, json));
                            AsyncMessageBox(result);
                            Close();
                        }
                        else if (SelectMethod == SelectMethodEnum.Post)
                        {
                            Result result = await Task.Run(() => RestAPI.Instance.Dish.Post(json));
                            AsyncMessageBox(result);
                            Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Группы не существует!");
                    }
                }
                else
                {
                    MessageBox.Show("Выбранное блюдо уже существует!");
                }
            }
            else
            {
                MessageBox.Show("Не все данные заполнены");
            }
        }

        private void bt_dobavhall_Click(object sender, EventArgs e)
        {
            Group hall = new Group();
            hall.SelectMethod = Group.SelectMethodEnum.Post;
            hall.ShowDialog();
            loadComboBox();
            cb_Group.Text = hall.Newid;
        }

        private void tb_price_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void tb_name_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
