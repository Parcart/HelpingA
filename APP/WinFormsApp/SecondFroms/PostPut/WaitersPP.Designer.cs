﻿namespace WinFormsApp.SecondFroms.PostPut
{
    partial class WaitersPP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_dobav = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_FIO = new System.Windows.Forms.TextBox();
            this.tb_telephone = new System.Windows.Forms.MaskedTextBox();
            this.tb_date = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_dobav
            // 
            this.btn_dobav.Location = new System.Drawing.Point(92, 135);
            this.btn_dobav.Name = "btn_dobav";
            this.btn_dobav.Size = new System.Drawing.Size(110, 53);
            this.btn_dobav.TabIndex = 1;
            this.btn_dobav.Text = "Добавить";
            this.btn_dobav.UseVisualStyleBackColor = true;
            this.btn_dobav.Click += new System.EventHandler(this.btn_dobav_ClickAsync);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(107, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 21);
            this.label1.TabIndex = 2;
            this.label1.Text = "Официант";
            // 
            // tb_FIO
            // 
            this.tb_FIO.Location = new System.Drawing.Point(119, 39);
            this.tb_FIO.Name = "tb_FIO";
            this.tb_FIO.Size = new System.Drawing.Size(165, 23);
            this.tb_FIO.TabIndex = 3;
            this.tb_FIO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_FIO_KeyPress);
            // 
            // tb_telephone
            // 
            this.tb_telephone.Location = new System.Drawing.Point(119, 68);
            this.tb_telephone.Mask = "+7 (999) 000-0000";
            this.tb_telephone.Name = "tb_telephone";
            this.tb_telephone.Size = new System.Drawing.Size(165, 23);
            this.tb_telephone.TabIndex = 4;
            // 
            // tb_date
            // 
            this.tb_date.Location = new System.Drawing.Point(119, 97);
            this.tb_date.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.tb_date.Name = "tb_date";
            this.tb_date.Size = new System.Drawing.Size(165, 23);
            this.tb_date.TabIndex = 10;
            this.tb_date.ValueChanged += new System.EventHandler(this.tb_date_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 15);
            this.label4.TabIndex = 17;
            this.label4.Text = "Дата рождения";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 15);
            this.label3.TabIndex = 16;
            this.label3.Text = "Номер телефона";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 15);
            this.label2.TabIndex = 15;
            this.label2.Text = "ФИО";
            // 
            // WaitersPP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 201);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_date);
            this.Controls.Add(this.tb_telephone);
            this.Controls.Add(this.tb_FIO);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_dobav);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WaitersPP";
            this.Text = "Создание зала";
            this.Load += new System.EventHandler(this.Hall_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_dobav;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_FIO;
        private System.Windows.Forms.MaskedTextBox tb_telephone;
        private System.Windows.Forms.DateTimePicker tb_date;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
    }
}