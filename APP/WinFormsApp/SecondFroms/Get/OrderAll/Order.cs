﻿using REST.Entity;
using REST;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Linq;
using WinFormsApp.SecondFroms.PostPut.OrderAll;

namespace WinFormsApp.SecondFroms.Get.OrderAll
{
    public partial class Order : Form
    {
        public Order()
        {
            InitializeComponent();
        }
        private async void Hall_Load(object sender, EventArgs e)
        {
            Result orderget = await Task.Run(() => RestAPI.Instance.Order.Get());
            List<Abstraction.Entity.Order> order = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Order>>(orderget.Data.ToString());

            Result clientget = await Task.Run(() => RestAPI.Instance.Client.Get());
            List<Abstraction.Entity.Client> client = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Client>>(clientget.Data.ToString());

            Result waitget = await Task.Run(() => RestAPI.Instance.Waiters.Get());
            List<Abstraction.Entity.Waiters> waiters = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Waiters>>(waitget.Data.ToString());

            List<Abstraction.Entity.OrderRest> orderRests = new List<Abstraction.Entity.OrderRest>();

            order.ForEach(cell =>
            {
                Abstraction.Entity.OrderRest orderRestsN = new Abstraction.Entity.OrderRest();
                if (cell.DateReservation != null)
                {
                    orderRestsN.DateReservation = cell.DateReservation;
                }
                    orderRestsN.Order_id = cell.Order_id;
                if(cell.client_id == null) 
                {
                    orderRestsN.clientName = cell.clientName;
                }
                else 
                {
                    orderRestsN.clientName = client.Where(cellF => cellF.Client_id == cell.client_id).FirstOrDefault().FIO;
                }
                orderRestsN.Data = cell.Data;
                orderRestsN.Waiters_id = waiters.Where(cellW => cellW.Waiter_id == cell.Waiter_ID).FirstOrDefault().FIO;
                if(cell.complite == "0")
                {
                    orderRestsN.complite = "Нет";
                }
                else { orderRestsN.complite = "Да"; }
                if (cell.IsReservation == "0") {orderRestsN.IsReservation = "Нет";}
                else { orderRestsN.IsReservation = "Да"; }
                orderRestsN.TotalPrice = cell.TotalPrice;
                orderRests.Add(orderRestsN);
            });


            dataGridView1.DataSource = orderRests;
            dataGridView1.Columns[0].HeaderText = "Номер заказа";
            dataGridView1.Columns[1].HeaderText = "Клиент";
            dataGridView1.Columns[2].HeaderText = "Дата";
            dataGridView1.Columns[3].HeaderText = "Официант";
            dataGridView1.Columns[4].Visible = false;
            dataGridView1.Columns[5].HeaderText = "Завершен";
            dataGridView1.Columns[6].HeaderText = "Бронь";
            dataGridView1.Columns[7].HeaderText = "Дата брони";
        }
        private void Hall_Load1()
        {
            Result orderget = RestAPI.Instance.Order.Get();
            List<Abstraction.Entity.Order> order = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Order>>(orderget.Data.ToString());

            Result clientget = RestAPI.Instance.Client.Get();
            List<Abstraction.Entity.Client> client = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Client>>(clientget.Data.ToString());

            Result waitget = RestAPI.Instance.Waiters.Get();
            List<Abstraction.Entity.Waiters> waiters = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Waiters>>(waitget.Data.ToString());

            List<Abstraction.Entity.OrderRest> orderRests = new List<Abstraction.Entity.OrderRest>();

            order.ForEach(cell =>
            {
                Abstraction.Entity.OrderRest orderRestsN = new Abstraction.Entity.OrderRest();
                if (cell.DateReservation != null)
                {
                    orderRestsN.DateReservation = cell.DateReservation;
                }
                orderRestsN.Order_id = cell.Order_id;
                if (cell.client_id == null)
                {
                    orderRestsN.clientName = cell.clientName;
                }
                else
                {
                    orderRestsN.clientName = client.Where(cellF => cellF.Client_id == cell.client_id).FirstOrDefault().FIO;
                }
                orderRestsN.Data = cell.Data;
                orderRestsN.Waiters_id = waiters.Where(cellW => cellW.Waiter_id == cell.Waiter_ID).FirstOrDefault().FIO;
                if (cell.complite == "0")
                {
                    orderRestsN.complite = "Нет";
                }
                else { orderRestsN.complite = "Да"; }
                if (cell.IsReservation == "0") { orderRestsN.IsReservation = "Нет"; }
                else { orderRestsN.IsReservation = "Да"; }
                orderRestsN.TotalPrice = cell.TotalPrice;
                orderRests.Add(orderRestsN);
            });


            dataGridView1.DataSource = orderRests;
            dataGridView1.Columns[0].HeaderText = "Номер заказа";
            dataGridView1.Columns[1].HeaderText = "Клиент";
            dataGridView1.Columns[2].HeaderText = "Дата";
            dataGridView1.Columns[3].HeaderText = "Официант";
            dataGridView1.Columns[4].Visible = false;
            dataGridView1.Columns[5].HeaderText = "Завершен";
            dataGridView1.Columns[6].HeaderText = "Бронь";
            dataGridView1.Columns[7].HeaderText = "Дата брони";
        }
        private void btn_post_Click(object sender, EventArgs e)
        {
            PostPut.OrderAll.OrderPP hall = new PostPut.OrderAll.OrderPP();
            hall.SelectMethod = PostPut.OrderAll.OrderPP.SelectMethodEnum.Post;
            hall.SelectOrder = OrderPP.SelectOrderWEnum.NO;
            hall.ShowDialog();
            Hall_Load1();
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (Resultid(dataGridView1.CurrentRow) != 0)
            {
                Result hallget = RestAPI.Instance.Hall.Delete((long)dataGridView1.CurrentRow.Cells[0].Value);
                if (hallget.Data.ToString() == "1")
                {
                    MessageBox.Show("Запись удалена", "Успешно");
                }
                else
                {
                    MessageBox.Show("Не удалось удалить запись", "Ошибка");
                }
                Hall_Load1();
            }
            else
            {
                MessageBox.Show("Строка не выбрана");
            }
        }

        private void btn_put_Click(object sender, EventArgs e)
        {
            if (Resultid(dataGridView1.CurrentRow) != 0)
            {
                PostPut.OrderAll.OrderPP hall = new PostPut.OrderAll.OrderPP();
                hall.SelectMethod = PostPut.OrderAll.OrderPP.SelectMethodEnum.Put;
                if (dataGridView1.CurrentRow.Cells[6].Value.ToString() == "Да")
                {
                    hall.SelectOrder = OrderPP.SelectOrderWEnum.Reserv;
                }
                else
                {
                    hall.SelectOrder = OrderPP.SelectOrderWEnum.NO;
                }
                hall.Selectid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
                hall.ShowDialog();
                Hall_Load1();
            }
            else
            {
                MessageBox.Show("Строка не выбрана");
            }
        }

        private int Resultid(object? row)
        {
            if (row != null)
            {
                int Selectid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
                return Selectid;
            }
            return Convert.ToInt32(row);
        }

        private void btn_COMPLITE_Click(object sender, EventArgs e)
        {
            PostPut.OrderAll.OrderPP hall = new PostPut.OrderAll.OrderPP();
            hall.SelectMethod = PostPut.OrderAll.OrderPP.SelectMethodEnum.Post;
            hall.SelectOrder = OrderPP.SelectOrderWEnum.Reserv;
            hall.ShowDialog();
            Hall_Load1();
        }
    }
}
