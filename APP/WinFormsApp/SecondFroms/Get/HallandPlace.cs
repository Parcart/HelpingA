﻿using REST;
using REST.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace WinFormsApp.SecondFroms.Get
{
    public partial class HallandPlace : Form
    {
        public HallandPlace()
        {
            InitializeComponent();
        }

        private void btn_Hall_Click(object sender, EventArgs e)
        {
            Hall hall = new Hall();
            hall.ShowDialog();
        }


        private void btn_Place_Click(object sender, EventArgs e)
        {
            PostPut.Places place = new PostPut.Places();
            place.SelectMethod = PostPut.Places.SelectMethodEnum.Post;
            place.ShowDialog();
            Hall_Load1();
        }

        private void HallandPlace_Load(object sender, EventArgs e)
        {
            Hall_Load1();
        }
        private void Hall_Load1()
        {
            Result Places = RestAPI.Instance.Places.Get();
            List<Abstraction.Entity.Places> places = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Places>>(Places.Data.ToString());

            Result hallget = RestAPI.Instance.Hall.Get();
            List<Abstraction.Entity.Hall> hall = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Hall>>(hallget.Data.ToString());

            List<Abstraction.Entity.PlacesRest> placesRests = new List<Abstraction.Entity.PlacesRest>();

            places.ForEach(cell =>
            {
                Abstraction.Entity.PlacesRest rest = new Abstraction.Entity.PlacesRest();
                rest.Table = cell.stolik;
                rest.Places_id = cell.Places_id;
                rest.Hall_id = hall.Where(cellhall => cellhall.Hall_id == cell.Hall_id).FirstOrDefault().Name;
                placesRests.Add(rest);
            });
            dataGridView1.DataSource = placesRests;
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].Visible = false;
            dataGridView1.Columns[3].HeaderText = "Столик";
            dataGridView1.Columns[2].HeaderText = "Зал";
            dataGridView1.Columns[4].Visible = false;
        }

        private void btn_put_Click(object sender, EventArgs e)
        {
            if (Resultid(dataGridView1.CurrentRow) != 0)
            {
                PostPut.Places place = new PostPut.Places();
                place.SelectMethod = PostPut.Places.SelectMethodEnum.Put;
                place.Selectid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
                place.ShowDialog();
                Hall_Load1();
            }
            else
            {
                MessageBox.Show("Строка не выбрана");
            }
        }
        private int Resultid(object? row)
        {
            if (row != null)
            {
                int Selectid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
                return Selectid;
            }
            return Convert.ToInt32(row);
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (Resultid(dataGridView1.CurrentRow) != 0)
            {
                Result hallget = RestAPI.Instance.Places.Delete((long)dataGridView1.CurrentRow.Cells[0].Value);
                if (hallget.Data != "0")
                {
                    MessageBox.Show("Успешно");
                }
                else
                {
                    MessageBox.Show("Ошибка");
                }
                Hall_Load1();
            }
            else
            {
                MessageBox.Show("Строка не выбрана");
            }
        }
    }
}
