﻿using REST.Entity;
using REST;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp.SecondFroms.Get
{
    public partial class Group : Form
    {
        public Group()
        {
            InitializeComponent();
        }
        private async void Hall_Load(object sender, EventArgs e)
        {
            Result hallget = await Task.Run(() => RestAPI.Instance.Group.Get());
            List<Abstraction.Entity.Group> hall = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Group>>(hallget.Data.ToString());
            dataGridView1.DataSource = hall;
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].HeaderText = "Название зала";
        }
        private void Hall_Load1()
        {
            Result hallget = RestAPI.Instance.Group.Get();
            List<Abstraction.Entity.Group> hall = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Group>>(hallget.Data.ToString());
            dataGridView1.DataSource = hall;
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].HeaderText = "Название зала";
        }
        private void btn_post_Click(object sender, EventArgs e)
        {
            PostPut.Group hall = new PostPut.Group();
            hall.SelectMethod = PostPut.Group.SelectMethodEnum.Post;
            hall.ShowDialog();
            Hall_Load1();
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (Resultid(dataGridView1.CurrentRow) != 0)
            {
                Result hallget = RestAPI.Instance.Group.Delete((long)dataGridView1.CurrentRow.Cells[0].Value);
                if (hallget.Data.ToString() == "1")
                {
                    MessageBox.Show("Запись удалена", "Успешно");
                }
                else
                {
                    MessageBox.Show("Не удалось удалить запись", "Ошибка");
                }
                Hall_Load1();
            }
            else
            {
                MessageBox.Show("Строка не выбрана");
            }
        }

        private void btn_put_Click(object sender, EventArgs e)
        {
            if (Resultid(dataGridView1.CurrentRow) != 0)
            {
                PostPut.Group hall = new PostPut.Group();
                hall.SelectMethod = PostPut.Group.SelectMethodEnum.Put;
                hall.Selectid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
                hall.ShowDialog();
                Hall_Load1();
            }
            else
            {
                MessageBox.Show("Строка не выбрана");
            }
        }

        private int Resultid(object? row)
        {
            if (row != null)
            {
                int Selectid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
                return Selectid;
            }
            return Convert.ToInt32(row);
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
