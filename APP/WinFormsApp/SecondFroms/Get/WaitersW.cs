﻿using REST.Entity;
using REST;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WinFormsApp.SecondFroms.Get
{
    public partial class WaitersW : Form
    {
        public WaitersW()
        {
            InitializeComponent();
        }

        private void WaitersW_Load(object sender, EventArgs e)
        {
            Hall_Load1();
        }
        private void Hall_Load1()
        {
            Result hallget = RestAPI.Instance.Waiters.Get();
            List<Abstraction.Entity.Waiters> hall = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Waiters>>(hallget.Data.ToString());
            dataGridView1.DataSource = hall;
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].HeaderText = "ФИО";
            dataGridView1.Columns[3].HeaderText = "Номер телефона";
            dataGridView1.Columns[2].HeaderText = "День рождения";
        }
        private void btn_post_Click(object sender, EventArgs e)
        {
            PostPut.WaitersPP hall = new PostPut.WaitersPP();
            hall.SelectMethod = PostPut.WaitersPP.SelectMethodEnum.Post;
            hall.ShowDialog();
            Hall_Load1();
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (Resultid(dataGridView1.CurrentRow) != 0)
            {
                Result hallget = RestAPI.Instance.Waiters.Delete((long)dataGridView1.CurrentRow.Cells[0].Value);
                if (hallget.Data.ToString() == "1")
                {
                    MessageBox.Show("Запись удалена", "Успешно");
                }
                else
                {
                    MessageBox.Show("Не удалось удалить запись", "Ошибка");
                }
                Hall_Load1();
            }
            else
            {
                MessageBox.Show("Строка не выбрана");
            }
        }

        private void btn_put_Click(object sender, EventArgs e)
        {
            if (Resultid(dataGridView1.CurrentRow) != 0)
            {
                PostPut.WaitersPP hall = new PostPut.WaitersPP();
                hall.SelectMethod = PostPut.WaitersPP.SelectMethodEnum.Put;
                hall.Selectid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
                hall.ShowDialog();
                Hall_Load1();
            }
            else
            {
                MessageBox.Show("Строка не выбрана");
            }
        }

        private int Resultid(object? row)
        {
            if (row != null)
            {
                int Selectid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
                return Selectid;
            }
            return Convert.ToInt32(row);
        }
    }
}
