﻿using REST.Entity;
using REST;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinFormsApp.SecondFroms.Get
{
    public partial class GroupAndDish : Form
    {
        public GroupAndDish()
        {
            InitializeComponent();
        }
        private void btn_Hall_Click(object sender, EventArgs e)
        {
            Group hall = new Group();
            hall.ShowDialog();
        }

        private void btn_Place_Click(object sender, EventArgs e)
        {
            PostPut.Dish place = new PostPut.Dish();
            place.SelectMethod = PostPut.Dish.SelectMethodEnum.Post;
            place.ShowDialog();
            Hall_Load1();
        }

        private void HallandPlace_Load(object sender, EventArgs e)
        {
            Hall_Load1();
        }
        private void Hall_Load1()
        {
            Result Dish = RestAPI.Instance.Dish.Get();
            List<Abstraction.Entity.Dish> places = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Dish>>(Dish.Data.ToString());

            Result Groupget = RestAPI.Instance.Group.Get();
            List<Abstraction.Entity.Group> hall = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Abstraction.Entity.Group>>(Groupget.Data.ToString());

            List<Abstraction.Entity.DishRest> placesRests = new List<Abstraction.Entity.DishRest>();

            places.ForEach(cell =>
            {
                Abstraction.Entity.DishRest rest = new Abstraction.Entity.DishRest();
                rest.Name = cell.Name;
                rest.Price = (long)cell.Price;
                rest.Dish_Id = cell.Dish_Id;
                rest.Group_id = hall.Where(cellhall => cellhall.Group_id == cell.Group_id).FirstOrDefault().Name;
                placesRests.Add(rest);
            });
            dataGridView1.DataSource = placesRests;
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[2].HeaderText = "Название";
            dataGridView1.Columns[3].HeaderText = "Цена";
            dataGridView1.Columns[1].HeaderText = "Группа";
        }

        private void btn_put_Click(object sender, EventArgs e)
        {
            if (Resultid(dataGridView1.CurrentRow) != 0)
            {
                PostPut.Dish place = new PostPut.Dish();
                place.SelectMethod = PostPut.Dish.SelectMethodEnum.Put;
                place.Selectid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
                place.ShowDialog();
                Hall_Load1();
            }
            else
            {
                MessageBox.Show("Строка не выбрана");
            }
        }
        private int Resultid(object? row)
        {
            if (row != null)
            {
                int Selectid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
                return Selectid;
            }
            return Convert.ToInt32(row);
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (Resultid(dataGridView1.CurrentRow) != 0)
            {
                Result hallget = RestAPI.Instance.Dish.Delete((long)dataGridView1.CurrentRow.Cells[0].Value);
                if (hallget.Data != "0")
                {
                    MessageBox.Show("Успешно");
                }
                else
                {
                    MessageBox.Show("Ошибка");
                }
                Hall_Load1();
            }
            else
            {
                MessageBox.Show("Строка не выбрана");
            }
        }
    }
}
