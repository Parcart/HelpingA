﻿namespace WinFormsApp
{
    partial class form_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.залToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.менюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.клиентыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.официантыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.заказыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_UPDATE = new System.Windows.Forms.Button();
            this.btn_UPDATEFORDATE = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.btn_POSTZAK = new System.Windows.Forms.Button();
            this.BT_ADDNEW = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_Posadka = new System.Windows.Forms.Button();
            this.BTN_PUT2 = new System.Windows.Forms.Button();
            this.btn_CREATERES = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.залToolStripMenuItem,
            this.менюToolStripMenuItem,
            this.клиентыToolStripMenuItem,
            this.официантыToolStripMenuItem,
            this.заказыToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1318, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // залToolStripMenuItem
            // 
            this.залToolStripMenuItem.Name = "залToolStripMenuItem";
            this.залToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.залToolStripMenuItem.Text = "Зал/места";
            this.залToolStripMenuItem.Click += new System.EventHandler(this.залToolStripMenuItem_Click);
            // 
            // менюToolStripMenuItem
            // 
            this.менюToolStripMenuItem.Name = "менюToolStripMenuItem";
            this.менюToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.менюToolStripMenuItem.Text = "Меню";
            this.менюToolStripMenuItem.Click += new System.EventHandler(this.менюToolStripMenuItem_Click);
            // 
            // клиентыToolStripMenuItem
            // 
            this.клиентыToolStripMenuItem.Name = "клиентыToolStripMenuItem";
            this.клиентыToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.клиентыToolStripMenuItem.Text = "Клиенты";
            this.клиентыToolStripMenuItem.Click += new System.EventHandler(this.клиентыToolStripMenuItem_Click);
            // 
            // официантыToolStripMenuItem
            // 
            this.официантыToolStripMenuItem.Name = "официантыToolStripMenuItem";
            this.официантыToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
            this.официантыToolStripMenuItem.Text = "Официанты";
            this.официантыToolStripMenuItem.Click += new System.EventHandler(this.официантыToolStripMenuItem_Click);
            // 
            // заказыToolStripMenuItem
            // 
            this.заказыToolStripMenuItem.Name = "заказыToolStripMenuItem";
            this.заказыToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.заказыToolStripMenuItem.Text = "Заказы";
            this.заказыToolStripMenuItem.Click += new System.EventHandler(this.заказыToolStripMenuItem_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer1.Size = new System.Drawing.Size(1318, 535);
            this.splitContainer1.SplitterDistance = 700;
            this.splitContainer1.TabIndex = 1;
            this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.label1);
            this.splitContainer2.Panel1.Controls.Add(this.btn_UPDATE);
            this.splitContainer2.Panel1.Controls.Add(this.btn_UPDATEFORDATE);
            this.splitContainer2.Panel1.Controls.Add(this.dateTimePicker1);
            this.splitContainer2.Panel1.Controls.Add(this.btn_POSTZAK);
            this.splitContainer2.Panel1.Controls.Add(this.BT_ADDNEW);
            this.splitContainer2.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer2_Panel1_Paint);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dataGridView1);
            this.splitContainer2.Size = new System.Drawing.Size(700, 535);
            this.splitContainer2.SplitterDistance = 74;
            this.splitContainer2.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(564, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 21);
            this.label1.TabIndex = 5;
            this.label1.Text = "Активные заказы";
            // 
            // btn_UPDATE
            // 
            this.btn_UPDATE.Location = new System.Drawing.Point(249, 32);
            this.btn_UPDATE.Name = "btn_UPDATE";
            this.btn_UPDATE.Size = new System.Drawing.Size(113, 43);
            this.btn_UPDATE.TabIndex = 4;
            this.btn_UPDATE.Text = "Обновить";
            this.btn_UPDATE.UseVisualStyleBackColor = true;
            this.btn_UPDATE.Click += new System.EventHandler(this.btn_UPDATE_Click);
            // 
            // btn_UPDATEFORDATE
            // 
            this.btn_UPDATEFORDATE.Location = new System.Drawing.Point(455, 5);
            this.btn_UPDATEFORDATE.Name = "btn_UPDATEFORDATE";
            this.btn_UPDATEFORDATE.Size = new System.Drawing.Size(75, 23);
            this.btn_UPDATEFORDATE.TabIndex = 3;
            this.btn_UPDATEFORDATE.Text = "Загрузить";
            this.btn_UPDATEFORDATE.UseVisualStyleBackColor = true;
            this.btn_UPDATEFORDATE.Click += new System.EventHandler(this.btn_UPDATEFORDATE_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(249, 3);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 23);
            this.dateTimePicker1.TabIndex = 2;
            // 
            // btn_POSTZAK
            // 
            this.btn_POSTZAK.Location = new System.Drawing.Point(126, 3);
            this.btn_POSTZAK.Name = "btn_POSTZAK";
            this.btn_POSTZAK.Size = new System.Drawing.Size(117, 72);
            this.btn_POSTZAK.TabIndex = 1;
            this.btn_POSTZAK.Text = "Изменить";
            this.btn_POSTZAK.UseVisualStyleBackColor = true;
            this.btn_POSTZAK.Click += new System.EventHandler(this.btn_POSTZAK_Click);
            // 
            // BT_ADDNEW
            // 
            this.BT_ADDNEW.Location = new System.Drawing.Point(3, 3);
            this.BT_ADDNEW.Name = "BT_ADDNEW";
            this.BT_ADDNEW.Size = new System.Drawing.Size(117, 72);
            this.BT_ADDNEW.TabIndex = 0;
            this.BT_ADDNEW.Text = "Создать заказ";
            this.BT_ADDNEW.UseVisualStyleBackColor = true;
            this.BT_ADDNEW.Click += new System.EventHandler(this.BT_ADDNEW_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(700, 457);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.label2);
            this.splitContainer3.Panel1.Controls.Add(this.btn_Posadka);
            this.splitContainer3.Panel1.Controls.Add(this.BTN_PUT2);
            this.splitContainer3.Panel1.Controls.Add(this.btn_CREATERES);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.dataGridView2);
            this.splitContainer3.Size = new System.Drawing.Size(614, 535);
            this.splitContainer3.SplitterDistance = 73;
            this.splitContainer3.TabIndex = 0;
            this.splitContainer3.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer3_SplitterMoved);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(487, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 21);
            this.label2.TabIndex = 6;
            this.label2.Text = "Бронирование";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // btn_Posadka
            // 
            this.btn_Posadka.Location = new System.Drawing.Point(249, 3);
            this.btn_Posadka.Name = "btn_Posadka";
            this.btn_Posadka.Size = new System.Drawing.Size(117, 72);
            this.btn_Posadka.TabIndex = 3;
            this.btn_Posadka.Text = "Активация";
            this.btn_Posadka.UseVisualStyleBackColor = true;
            this.btn_Posadka.Click += new System.EventHandler(this.btn_Posadka_Click);
            // 
            // BTN_PUT2
            // 
            this.BTN_PUT2.Location = new System.Drawing.Point(126, 2);
            this.BTN_PUT2.Name = "BTN_PUT2";
            this.BTN_PUT2.Size = new System.Drawing.Size(117, 73);
            this.BTN_PUT2.TabIndex = 2;
            this.BTN_PUT2.Text = "Изменить";
            this.BTN_PUT2.UseVisualStyleBackColor = true;
            this.BTN_PUT2.Click += new System.EventHandler(this.BTN_PUT2_Click);
            // 
            // btn_CREATERES
            // 
            this.btn_CREATERES.Location = new System.Drawing.Point(3, 3);
            this.btn_CREATERES.Name = "btn_CREATERES";
            this.btn_CREATERES.Size = new System.Drawing.Size(117, 72);
            this.btn_CREATERES.TabIndex = 1;
            this.btn_CREATERES.Text = "Создать бронь";
            this.btn_CREATERES.UseVisualStyleBackColor = true;
            this.btn_CREATERES.Click += new System.EventHandler(this.btn_CREATERES_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(0, 0);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(614, 458);
            this.dataGridView2.TabIndex = 0;
            // 
            // form_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1318, 559);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(1334, 598);
            this.Name = "form_main";
            this.Text = "Главная страница";
            this.Load += new System.EventHandler(this.form_main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel1.PerformLayout();
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.ToolStripMenuItem залToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem менюToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem клиентыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem официантыToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.ToolStripMenuItem заказыToolStripMenuItem;
        private System.Windows.Forms.Button BT_ADDNEW;
        private System.Windows.Forms.Button btn_POSTZAK;
        private System.Windows.Forms.Button btn_Posadka;
        private System.Windows.Forms.Button BTN_PUT2;
        private System.Windows.Forms.Button btn_CREATERES;
        private System.Windows.Forms.Button btn_UPDATE;
        private System.Windows.Forms.Button btn_UPDATEFORDATE;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}